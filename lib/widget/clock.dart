import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

class Clock extends StatefulWidget {
  Clock({Key key}) : super(key: key);

  @override
  _ClockState createState() => _ClockState();
}

class _ClockState extends State<Clock> {
  String _timeString;

  @override
  void initState() {
    super.initState();
    initializeDateFormatting();
    _timeString = _formatDateTime(DateTime.now());
    Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Text(
      _timeString,
      style: TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.w600,
          fontStyle: FontStyle.italic),
    );
  }

  void _getTime() {
    final DateTime now = DateTime.now();
    final String formattedDateTime = _formatDateTime(now);
     if (!mounted) return;
    setState(() {
      _timeString = formattedDateTime;
    });
  }

  String _formatDateTime(DateTime dateTime) {
    if(dateTime.second.isEven){
      return DateFormat('hh:mm').format(dateTime);
    }else{
      return DateFormat('hh mm').format(dateTime);
    }
    
  }
}
