import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_app_1/constant.dart';

class ErrorWidget extends StatefulWidget {
  ErrorWidget({Key key}) : super(key: key);

  @override
  _ErrorWidget createState() => _ErrorWidget();
}

class _ErrorWidget extends State<ErrorWidget> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    var _height = UI.height(context);
    var _width = UI.width(context);
    return Container(
      height: _height,
      width: _width,

    );
  }
}
