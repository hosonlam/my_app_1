import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:my_app_1/constant.dart';
import 'package:my_app_1/screen/account_store/account.dart';
import 'package:my_app_1/screen/base/frame.dart';
import 'package:my_app_1/screen/compass.dart';
import 'package:my_app_1/screen/covid.dart';
import 'package:my_app_1/screen/fund/fund.dart';
import 'package:my_app_1/screen/settings.dart';
import 'package:my_app_1/screen/weather/weather.dart';
import 'package:my_app_1/utils/lunar_convert.dart';
import 'package:my_app_1/utils/static.dart';

class DrawerCustom extends StatefulWidget {
  final bottomNavigationKey;
  DrawerCustom({Key key, this.bottomNavigationKey}) : super(key: key);

  @override
  _DrawerCustomState createState() => _DrawerCustomState();
}

class _DrawerCustomState extends State<DrawerCustom> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    final CurvedNavigationBarState navBarState =
        widget.bottomNavigationKey.currentState;
    List<ItemList> items = [
      ItemList()
        ..icon = Icons.info
        ..color = Colors.grey[800]
        ..title = tr("drawer_info")
        ..page = 0
        ..screen = null,
      ItemList()
        ..icon = Icons.flash_on
        ..color = Colors.amber[800]
        ..title = tr("drawer_electric")
        ..page = 1
        ..screen = null,
      ItemList()
        ..icon = Icons.opacity
        ..color = Colors.blue[800]
        ..title = tr("drawer_water")
        ..page = 2
        ..screen = null,
      ItemList()
        ..icon = Icons.attach_money
        ..color = Colors.green[800]
        ..title = tr("drawer_money")
        ..page = 3
        ..screen = null,
      ItemList()
        ..icon = Icons.kitchen
        ..color = Colors.cyan[500]
        ..title = tr("drawer_fund")
        ..page = -1
        ..screen = FundScreen(),
      ItemList()
        ..icon = Icons.cloud
        ..color = Colors.lightBlue[500]
        ..title = tr("drawer_weather")
        ..page = -1
        ..screen = WeatherScreen(),
      ItemList()
        ..icon = Icons.my_location
        ..color = Colors.purple[500]
        ..title = tr("drawer_compass")
        ..page = -1
        ..screen = CompassScreen(),
      ItemList()
        ..icon = Icons.bug_report
        ..color = Colors.red[800]
        ..title = tr("drawer_covid")
        ..page = -1
        ..screen = CovidScreen(),
      ItemList()
        ..icon = Icons.lock
        ..color = Colors.purple[800]
        ..title = tr("drawer_account")
        ..page = -1
        ..screen = AccountScreen(),
    ];
    return Drawer(
        child: Column(
      children: <Widget>[
        Expanded(
          flex: 4,
          child: ListView(
            physics: const NeverScrollableScrollPhysics(),
            children: <Widget>[
              Container(
                height: 200,
                child: DrawerHeader(
                  padding: EdgeInsets.zero,
                  child: Container(
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            colorFilter: ColorFilter.mode(
                                Colors.black.withOpacity(0.4),
                                BlendMode.darken),
                            fit: BoxFit.cover,
                            image: ExactAssetImage(Season
                                .getBackgroundSeason[StaticVariable.season]))),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              _timer(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w600),
                            ),
                            IconButton(
                              icon: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                setState(() {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Frame()));
                                });
                              },
                            )
                          ],
                        ),
                        Text(
                          '${_weekDay()},',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.w500),
                        ),
                        Text(
                          'Dương lịch : ${_positiveDay()}',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.w400),
                        ),
                        Text(
                          'Âm lịch : ${_negativeDay()}',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.w400),
                        ),
                        SizedBox()
                      ],
                    ),
                  ),
                  decoration: BoxDecoration(color: Colors.green[400]),
                ),
              ),
              ListView.builder(
                  shrinkWrap: true,
                  itemCount: items.length,
                  itemBuilder: (context, index) {
                    return _listItem(
                        icon: items[index].icon,
                        color: items[index].color,
                        title: items[index].title,
                        onTap: () {
                          setState(() {
                            Navigator.pop(context);
                            if (items[index].page >= 0 &&
                                items[index].screen == null) {
                              navBarState.setPage(items[index].page);
                            } else {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          items[index].screen));
                            }
                          });
                        });
                  }),
            ],
          ),
        ),
        InkWell(
          onTap: () {
            setState(() {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SettingScreen()));
            });
          },
          child: Container(
            padding: EdgeInsets.all(20),
            child: Row(
              children: <Widget>[
                Icon(Icons.settings, color: Colors.grey[800]),
                SizedBox(width: 30),
                Text(tr("drawer_setting"),style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500),)
              ],
            ),
          ),
        )
      ],
    ));
  }

  Widget _listItem({IconData icon, Color color, String title, Function onTap}) {
    return ListTile(
        leading: Icon(icon, color: color),
        title: Text(title, style: TextStyle(fontSize: 16)),
        onTap: onTap);
  }

  String _timer() {
    var hour = DateTime.now().hour;
    String message = "";
    if (hour >= 0 && hour < 5) message = "Chào buổi tối";
    if (hour >= 5 && hour < 12) message = "Chào buổi sáng";
    if (hour >= 12 && hour < 17) message = "Chào buổi chiều";
    if (hour >= 17 && hour < 24) message = "Chào buổi tối";
    return message;
  }

  String _weekDay() {
    var day = DateTime.now().weekday;
    switch (day) {
      case 1:
        return "Thứ 2";
        break;

      case 2:
        return "Thứ 3";
        break;

      case 3:
        return "Thứ 4";
        break;

      case 4:
        return "Thứ 5";
        break;

      case 5:
        return "Thứ 6";
        break;

      case 6:
        return "Thứ 7";
        break;

      case 7:
        return "Chủ nhật";
        break;

      default:
        return "";
    }
  }

  String _positiveDay() {
    var positive = DateFormat('dd/MM/yyyy').format(DateTime.now());
    return positive;
  }

  String _negativeDay() {
    DateTime now = DateTime.now();
    var negatives = convertSolar2Lunar(now.day, now.month, now.year, 7);
    String day = '${negatives[0]}/${negatives[1]}/${negatives[2]}';
    return day;
  }
}

class ItemList {
  Color color;
  IconData icon;
  String title;
  int page;
  dynamic screen;

  ItemList();
}
