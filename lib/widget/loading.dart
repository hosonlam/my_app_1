import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_app_1/widget/loading_state.dart';

class LoadingWidget extends StatefulWidget {
  @override
  State createState() => _LoadingWidgetState();
}

class _LoadingWidgetState extends State<LoadingWidget>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  child: Text(
                "Vui lòng chờ đợi.",
                style: TextStyle(fontSize: 16),
              )),
              SizedBox(height: 20),
              Container(color: Colors.white, child: LoadingStateWidget()),
              SizedBox(height: 50),
              Container(
                width: 200,
                height: 50,
                decoration: BoxDecoration(
                    color: Colors.green[400],
                    borderRadius: BorderRadius.circular(12)),
                child: FlatButton(
                  child: Text(
                    "Quay lại",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    setState(() {
                      Navigator.pop(context);
                    });
                  },
                ),
              )
            ],
          ),
        ),
      );
  }

}