import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class DialogUtilities {
  @protected
  static const int Info = 0;

  @protected
  static const int Warning = 1;

  @protected
  static const int Error = 2;

  @protected
  static const int Success = 3;

  static void showError(BuildContext context, String message,
      {String title = "",
      bool isLock = false,
      String buttonTitle,
      Function cancelAction,
      Function action}) {
    buildMessage(context, Error, message,
        cancelAction: cancelAction,
        title: title,
        buttonTitle: buttonTitle,
        action: action,
        isLock: isLock);
  }

  static void showWarning(BuildContext context, String message,
      {String title = "", String buttonTitle, Function action}) {
    buildMessage(context, Warning, message,
        title: title, buttonTitle: buttonTitle, action: action, isLock: false);
  }

  static void showInfo(BuildContext context, String message,
      {String title = "", String buttonTitle, Function action}) {
    buildMessage(context, Info, message,
        title: title, buttonTitle: buttonTitle, action: action, isLock: false);
  }

  static void showConfirm(BuildContext context, String message,
      {String title = "",
      String buttonTitle,
      Function action,
      String buttonCancelTitle,
      Function cancelAction,
      bool isLock}) {
    buildMessage(context, Warning, message,
        title: title,
        buttonTitle: buttonTitle,
        action: action,
        buttonCancelTitle: buttonCancelTitle,
        cancelAction: cancelAction,
        isLock: isLock ?? false,
        isConfirm: true);
  }

  static void showSuccess(BuildContext context, String message,
      {String title = "", String buttonTitle, Function action}) {
    buildMessage(context, Success, message,
        title: title, buttonTitle: buttonTitle, action: action, isLock: false);
  }

  @protected
  static void buildMessage(
      BuildContext context, int messageType, String message,
      {bool isConfirm = false,
      String title = "",
      String buttonTitle,
      Function action,
      bool isLock,
      String buttonCancelTitle,
      Function cancelAction}) {
    final screenWidth = MediaQuery.of(context).size.width;
    final maxWidth = screenWidth > 600.0 ? screenWidth * 0.7 : screenWidth;
    var alertStyle = AlertStyle(
      isCloseButton: !isLock,
      isOverlayTapDismiss: !isLock,
      constraints: BoxConstraints(maxWidth: maxWidth),
    );

    var type = messageType == Info
        ? AlertType.info
        : messageType == Error
            ? AlertType.error
            : messageType == Warning ? AlertType.warning : AlertType.success;
    var buttons = [
      DialogButton(
        child: Text(
          buttonTitle ?? "OK",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: action ?? () => Navigator.pop(context),
        //width: 120,
      )
    ];

    if (isConfirm == true) {
      buttons.add(DialogButton(
        child: Text(
          buttonCancelTitle ?? "Cancel",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: cancelAction ?? () => Navigator.pop(context),
        gradient: LinearGradient(colors: [
          Color.fromRGBO(116, 116, 191, 1.0),
          Color.fromRGBO(52, 138, 199, 1.0)
        ]),
        //width: 120,
      ));
    }

    var content = Container(
      constraints: BoxConstraints(maxWidth: maxWidth),
      child: Text(message,
          style: AlertStyle().descStyle, textAlign: TextAlign.center),
    );

    Alert(
            context: context,
            type: type,
            style: alertStyle,
            title: title,
            // desc: message,
            content: content,
            buttons: buttons)
        .show();
  }
}
