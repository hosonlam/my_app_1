import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ShowAlertDialog {
  ShowAlertDialog.oneButton(
      BuildContext context, String title, String message) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {},
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("$title"),
      content: Text("$message"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  ShowAlertDialog.twoButton(
      {BuildContext context,
      String title,
      String message,
      String agree,
      String disagree,
      Function onPressed}) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("$disagree"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = FlatButton(
      child: Text("$agree"),
      onPressed: () {
        onPressed();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("$title"),
      content: Text("$message"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
