import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_app_1/constant.dart';
import 'package:my_app_1/utils/static.dart';

class BottomNavigator extends StatefulWidget {
  final bottomNavigationKey;
  final pageController;
  BottomNavigator({Key key, this.bottomNavigationKey, this.pageController})
      : super(key: key);

  @override
  _BottomNavigatorState createState() => _BottomNavigatorState();
}

class _BottomNavigatorState extends State<BottomNavigator> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    var _height = UI.height(context);
    return Container(
      height: _height * 0.1,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
            Season.getColors[StaticVariable.season],
            Season.getColors[StaticVariable.season],
            Colors.white,
          ])),
      child: CurvedNavigationBar(
        key: widget.bottomNavigationKey,
        backgroundColor: Colors.transparent,
        items: <Widget>[
          Icon(Icons.info, size: 30),
          Icon(Icons.flash_on, size: 30),
          Icon(Icons.home, size: 30),
          Icon(Icons.opacity, size: 30),
          Icon(Icons.attach_money, size: 30),
        ],
        onTap: (index) {
          setState(() {
            widget.pageController.animateToPage(index,
                duration: Duration(microseconds: 250), curve: Curves.bounceOut);
          });
        },
      ),
    );
  }
}
