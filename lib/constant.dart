

import 'package:flutter/widgets.dart';

class UI {
  static width(BuildContext context) => MediaQuery.of(context).size.width;
  static height(BuildContext context) => MediaQuery.of(context).size.height;
}

class WeatherCondition {
  static final String thunderstorm = "thunderstorm";
  static final String drizzle = "drizzle";
  static final String rain = "rain";
  static final String snow = "snow";
  static final String atmosphere = "atmosphere";
  static final String clear = "clear";
  static final String clouds = "clouds";

  static Map<String, String> getBackground = {
    thunderstorm: 'assets/images/storm.png',
    drizzle: 'assets/images/rain.png',
    rain: 'assets/images/rain.png',
    snow: 'assets/images/snow.png',
    clouds: 'assets/images/cloud.png',
    clear: 'assets/images/clear.png',
    atmosphere: 'assets/images/mist.png',
  };
}

class Model {

  //Fund
  static final String tableFund = "fund";
  static final String colIdFund = "id";
  static final String colAmountFund = "amount";
  static final String colNoteFund = "note";
  static final String colDateFund = "date";

  //Account
  static final String tableAccount = "account";
  static final String colIdAccount = "id";
  static final String colPass = "password";

  //AccountStore
  static final String tableAccountStore = "account_store";
  static final String colIdAccountStore = "id";
  static final String colTitleAccountStore = "title";
  static final String colUsernameAccountStore = "username";
  static final String colPasswordAccountStore = "password";
  static final String colNoteAccountStore = "note";
  static final String colIconAccountStore = "icon";

  //Customer
  static final String tableCustomer = "customer";
  static final String colIdCustomer = "id";
  static final String colFullNameCustomer = "fullname";
  static final String colAgeCustomer = "age";
  static final String colPhoneCustomer = "phone";
  static final String colPhone2Customer = "phone2";
  static final String colEmailCustomer = "email";
  static final String colSexCustomer = "sex";
  static final String colAddressCustomer = "address";
  static final String colIdCardCustomer = "idCard";
  static final String colBirthDayCustomer = "birthDay";
  static final String colBirthPlaceCustomer = "birthPlace";

  //Water
  static final String tableWater = "water";
  static final String colIdWater = "id";
  static final String colDateWater = "date";
  static final String colPriceWater = "price";
  static final String colStatusWater = "status";
  static final String colNumberWater = "number";
  static final String colSumWater = "sum";
  static final String colCusIdWater = "idCustomer";

//Electric
  static final String tableElectric = "electric";
  static final String colIdElectric = "id";
  static final String colDateElectric = "date";
  static final String colPriceElectric = "price";
  static final String colStatusElectric = "status";
  static final String colNumberElectric = "number";
  static final String colSumElectric = "sum";
  static final String colCusIdElectric = "idCustomer";

//Debit
  static final String tableDebit = "debit";
  static final String colIdDebit = "id";
  static final String colNoteDebit = "note";
  static final String colCusId = "idCustomer";
}

class RefConstant {
  static final String weatherName = "weather";
  static final String weatherTime = "weatherTime";
  static final String forecastName = "forecast";
  static final String forecastTime = "forecastTime";
}

class Season {
  static final String spring = "spring";
  static final String summer = "summer";
  static final String autumn = "autumn";
  static final String winter = "winter";

  static final int springMonth = 3;
  static final int summerMonth = 6;
  static final int autumnMonth = 9;
  static final int winterMonth = 12;

  static Map<String, String> getBackgroundSeason = {
    spring: 'assets/images/spring.png',
    summer: 'assets/images/summer.png',
    autumn: 'assets/images/autumn.png',
    winter: 'assets/images/winter.png',
  };

  static Map<String, dynamic> getColors = {
    spring: Color.fromRGBO(233, 177, 205, 1),
    summer: Color.fromRGBO(138, 211, 33, 1),
    autumn: Color.fromRGBO(248, 157, 19, 1),
    winter: Color.fromRGBO(166, 177, 225, 1),
  };
}
