import 'package:my_app_1/constant.dart';
import 'package:my_app_1/database/master_data.dart';

class AccountStoreModel {
  int id;
  String title;
  String username;
  String password;
  String note;
  String icon;

  AccountStoreModel();

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      Model.colTitleAccountStore: title,
      Model.colUsernameAccountStore: username,
      Model.colPasswordAccountStore: password,
      Model.colNoteAccountStore: note,
      Model.colIconAccountStore: icon,
    };
    if (id != null) {
      map[Model.colIdAccountStore] = id;
    }
    return map;
  }

  AccountStoreModel.fromMap(Map<String, dynamic> map) {
    id = map[Model.colIdAccountStore];
    username = map[Model.colUsernameAccountStore];
    password = map[Model.colPasswordAccountStore];
    title = map[Model.colTitleAccountStore];
    note = map[Model.colNoteAccountStore];
    icon = map[Model.colIconAccountStore];
  }
}

class AccountStoreProvider {
  Future<AccountStoreModel> insert(AccountStoreModel account) async {
    account.id =
        await MasterData.db.insert(Model.tableAccountStore, account.toMap());
    return account;
  }

  Future<List<AccountStoreModel>> getAll() async {
    List<Map> maps = await MasterData.db.query(Model.tableAccountStore,
        orderBy: "${Model.colTitleAccountStore} ASC");
    if (maps != null) {
      List<AccountStoreModel> accounts = List();
      maps.forEach((e) => accounts.add(AccountStoreModel.fromMap(e)));
      return accounts;
    }
    return null;
  }

  Future<AccountStoreModel> getAccountById(int id) async {
    var res = await MasterData.db.query(Model.tableAccountStore,
        where: '${Model.colIdAccountStore} = ?', whereArgs: [id]);
    return res.isNotEmpty ? AccountStoreModel.fromMap(res.first) : null;
  }

  Future<int> delete(int id) async {
    return await MasterData.db.delete(Model.tableAccountStore,
        where: '${Model.colIdAccountStore} = ?', whereArgs: [id]);
  }

  Future<int> update(AccountStoreModel account) async {
    return await MasterData.db.update(Model.tableAccountStore, account.toMap(),
        where: '${Model.colIdAccountStore} = ?', whereArgs: [account.id]);
  }

  Future close() async => MasterData.db.close();
}
