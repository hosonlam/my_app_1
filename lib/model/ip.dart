class IpModel {
  String ip;
  Location location;
  As as;
  String isp;

  IpModel({this.ip, this.location, this.as, this.isp});

  IpModel.fromJson(Map<String, dynamic> json) {
    ip = json['ip'];
    location = json['location'] != null? new Location.fromJson(json['location']): null;
    as = json['as'] != null ? new As.fromJson(json['as']) : null;
    isp = json['isp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ip'] = this.ip;
    if (this.location != null) {
      data['location'] = this.location.toJson();
    }
    if (this.as != null) {
      data['as'] = this.as.toJson();
    }
    data['isp'] = this.isp;
    return data;
  }
}

class Location {
  String country;
  String region;
  String city;
  double lat;
  double lng;
  String postalCode;
  String timezone;
  int geonameId;

  Location(
      {this.country,
      this.region,
      this.city,
      this.lat,
      this.lng,
      this.postalCode,
      this.timezone,
      this.geonameId});

  Location.fromJson(Map<String, dynamic> json) {
    country = json['country'];
    region = json['region'];
    city = json['city'];
    lat = json['lat'];
    lng = json['lng'];
    postalCode = json['postalCode'];
    timezone = json['timezone'];
    geonameId = json['geonameId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['country'] = this.country;
    data['region'] = this.region;
    data['city'] = this.city;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['postalCode'] = this.postalCode;
    data['timezone'] = this.timezone;
    data['geonameId'] = this.geonameId;
    return data;
  }
}

class As {
  int asn;
  String name;
  String route;
  String domain;
  String type;

  As({this.asn, this.name, this.route, this.domain, this.type});

  As.fromJson(Map<String, dynamic> json) {
    asn = json['asn'];
    name = json['name'];
    route = json['route'];
    domain = json['domain'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['asn'] = this.asn;
    data['name'] = this.name;
    data['route'] = this.route;
    data['domain'] = this.domain;
    data['type'] = this.type;
    return data;
  }
}
