import 'package:my_app_1/constant.dart';
import 'package:my_app_1/database/master_data.dart';

class FundModel {
  int id;
  double amount;
  DateTime date;
  String note;

  FundModel();

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      Model.colAmountFund: amount,
      Model.colDateFund: date,
      Model.colNoteFund: note,
    };
    if (id != null) {
      map[Model.colIdFund] = id;
    }
    return map;
  }

  FundModel.fromMap(Map<String, dynamic> map) {
    id = map[Model.colIdFund];
    amount = map[Model.colAmountFund];
    date = map[Model.colDateFund];
    note = map[Model.colNoteFund];
  }
}

class FundProvider {
  Future<List<FundModel>> getAll() async {
    List<Map> maps = await MasterData.db
        .query(Model.tableFund, orderBy: "${Model.colDateFund} ASC");
    if (maps != null) {
      List<FundModel> funds = List();
      maps.forEach((e) => funds.add(FundModel.fromMap(e)));
      return funds;
    }
    return null;
  }

  Future<FundModel> getCustomerbyId(int id) async {
    var res = await MasterData.db.query(Model.tableFund,
        where: '${Model.colIdFund} = ?', whereArgs: [id]);
    return res.isNotEmpty ? FundModel.fromMap(res.first) : null;
  }

  Future<List<FundModel>> getFundByMonth(int month) async {
    List<Map> maps = await MasterData.db.query(Model.tableFund,
        where: ' strftime(%m, ${Model.colDateFund}) = ?',
        whereArgs: [month],
        orderBy: "${Model.colDateFund}");
    if (maps != null) {
      List<FundModel> funds = List();
      maps.forEach((e) => funds.add(FundModel.fromMap(e)));
      return funds;
    } else {
      return null;
    }
  }

  Future<List<FundModel>> getFundByYear(int year) async {
    List<Map> maps = await MasterData.db.query(Model.tableFund,
        where: ' strftime(%Y, ${Model.colDateFund}) = ?',
        whereArgs: [year],
        orderBy: "${Model.colDateFund}");
    if (maps != null) {
      List<FundModel> funds = List();
      maps.forEach((e) => funds.add(FundModel.fromMap(e)));
      return funds;
    } else {
      return null;
    }
  }
}
