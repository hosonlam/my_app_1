import 'package:my_app_1/constant.dart';
import 'package:my_app_1/database/master_data.dart';

class AccountModel {
  String id;
  String password;

  AccountModel();
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      Model.colIdAccount: id,
      Model.colPass: password,
    };
    if (id != null) {
      map[Model.colIdAccount] = id;
    }
    return map;
  }

  AccountModel.fromMap(Map<String, dynamic> map) {
    id = map[Model.colIdAccount];
    password = map[Model.colPass];
  }
}

class AccountProvider {
  Future<AccountModel> getAccountbyId(String id) async {
    var res = await MasterData.db.query(Model.tableAccount,
        where: '${Model.colIdAccount} = ?', whereArgs: [id]);
    return res.isNotEmpty ? AccountModel.fromMap(res.first) : null;
  }
}
