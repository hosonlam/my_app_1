import 'package:my_app_1/constant.dart';
import 'package:my_app_1/database/master_data.dart';

class CustomerModel {
  int id;
  String fullName;
  String age;
  String phone;
  String phone2;
  String email;
  int sex;
  String address;
  String idCard;
  String birthDay;
  String birthPlace;

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      Model.colFullNameCustomer: fullName,
      Model.colAgeCustomer: age,
      Model.colPhoneCustomer: phone,
      Model.colPhone2Customer: phone2,
      Model.colEmailCustomer: email,
      Model.colSexCustomer: sex,
      Model.colAddressCustomer: address,
      Model.colIdCardCustomer: idCard,
      Model.colBirthDayCustomer: birthDay,
      Model.colBirthPlaceCustomer: birthPlace
    };
    if (id != null) {
      map[Model.colIdCustomer] = id;
    }
    return map;
  }

  CustomerModel();

  CustomerModel.fromMap(Map<String, dynamic> map) {
    id = map[Model.colIdCustomer];
    fullName = map[Model.colFullNameCustomer];
    age = map[Model.colAgeCustomer];
    phone = map[Model.colPhoneCustomer];
    phone2 = map[Model.colPhone2Customer];
    email = map[Model.colEmailCustomer];
    sex = map[Model.colSexCustomer];
    address = map[Model.colAddressCustomer];
    idCard = map[Model.colIdCardCustomer];
    birthDay = map[Model.colBirthDayCustomer];
    birthPlace = map[Model.colBirthPlaceCustomer];
  }
}

class CustomerProvider {
  Future<CustomerModel> insert(CustomerModel customer) async {
    customer.id =
        await MasterData.db.insert(Model.tableCustomer, customer.toMap());
    return customer;
  }

  Future<List<CustomerModel>> getAll() async {
    List<Map> maps = await MasterData.db.query(Model.tableCustomer, orderBy: "${Model.colFullNameCustomer} ASC");
    if(maps != null){
      List<CustomerModel> customers = List();
      maps.forEach((e) => customers.add(CustomerModel.fromMap(e)));
      return customers;
    }
    return null;
  }

  Future<CustomerModel> getCustomerbyId(int id) async {
    var res = await MasterData.db.query(Model.tableCustomer,
        where: '${Model.colIdCustomer} = ?', whereArgs: [id]);
    return res.isNotEmpty ? CustomerModel.fromMap(res.first) : null;
  }

  Future<int> delete(int id) async {
    return await MasterData.db.delete(Model.tableCustomer,
        where: '${Model.colIdCustomer} = ?', whereArgs: [id]);
  }

  Future<int> update(CustomerModel customer) async {
    return await MasterData.db.update(Model.tableCustomer, customer.toMap(),
        where: '${Model.colIdCustomer} = ?', whereArgs: [customer.id]);
  }

  Future close() async => MasterData.db.close();
}
