class ConvertTimestamp {
  static DateTime convert(int timestamp,int offset) {
    return DateTime.fromMillisecondsSinceEpoch((timestamp + offset) * 1000)
        .toUtc();
  }
}
