import 'package:my_app_1/constant.dart';

class SeasonCalculate {
  String getSeason() {
    DateTime now = DateTime.now();
    int nowMonth = now.month;

    if (nowMonth >= Season.springMonth && nowMonth < Season.summerMonth) {
      return Season.spring;
    } else if (nowMonth >= Season.summerMonth && nowMonth < Season.autumnMonth) {
      return Season.summer;
    } else if (nowMonth >= Season.autumnMonth && nowMonth < Season.winterMonth) {
      return Season.autumn;
    } else
      return Season.winter;
  }
}
