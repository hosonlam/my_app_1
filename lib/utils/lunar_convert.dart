/*
 * Copyright (c) 2006 Ho Ngoc Duc. All Rights Reserved.
 * Astronomical algorithms from the book "Astronomical Algorithms" by Jean Meeus, 1998
 *
 * Permission to use, copy, modify, and redistribute this software and its
 * documentation for personal, non-commercial use is hereby granted provided that
 * this copyright notice and appropriate documentation appears in all copies.
 */
import 'dart:math';

var pI = pi;

const canList = [
  "Canh",
  "Tân",
  "Nhâm",
  "Quý",
  "Giáp",
  "Ất",
  "Bính",
  "Đinh",
  "Mậu",
  "Kỉ"
];
const chiList = [
  "Thân",
  "Dậu",
  "Tuất",
  "Hợi",
  "Tý",
  "Sửu",
  "Dần",
  "Mẹo",
  "Thìn",
  "Tị",
  "Ngọ",
  "Mùi"
];

const chiForMonthList = [
  "Dần",
  "Mẹo",
  "Thìn",
  "Tị",
  "Ngọ",
  "Mùi",
  "Thân",
  "Dậu",
  "Tuất",
  "Hợi",
  "Tý",
  "Sửu",
];

const CAN = ['Giáp', 'Ất', 'Bính', 'Đinh', 'Mậu', 'Kỷ', 'Canh', 'Tân', 'Nhâm', 'Quý'];
const CHI = ['Tý', 'Sửu', 'Dần', 'Mẹo', 'Thìn', 'Tỵ', 'Ngọ', 'Mùi', 'Thân', 'Dậu', 'Tuất', 'Hợi'];
const TIETKHI = ['Xuân phân', 'Thanh minh', 'Cốc vũ', 'Lập hạ', 'Tiểu mãn', 'Mang chủng',
'Hạ chí', 'Tiểu thử', 'Đại thử', 'Lập thu', 'Xử thử', 'Bạch lộ',
'Thu phân', 'Hàn lộ', 'Sương giáng', 'Lập đông', 'Tiểu tuyết', 'Đại tuyết',
'Đông chí', 'Tiểu hàn', 'Đại hàn', 'Lập xuân', 'Vũ thủy', 'Kinh trập'
];
const GIO_HD = ['110100101100', '001101001011', '110011010010', '101100110100', '001011001101', '010010110011'];


/* Discard the fractional part of a number, e.g., _int(3.2) = 3 */
_int(double d) {
  return d.toInt();
}

/* Compute the (integral) Julian day number of day dd/mm/yyyy, i.e., the number 
 * of days between 1/1/4713 BC (Julian calendar) and dd/mm/yyyy. 
 * Formula from http://www.tondering.dk/claus/calendar.html
 */
jdFromDate(dd, mm, yy) {
  var a, y, m, jd;
  a = _int((14 - mm) / 12);
  y = yy + 4800 - a;
  m = mm + 12 * a - 3;
  jd = dd + _int((153 * m + 2) / 5) + 365 * y + _int(y / 4) - _int(y / 100) +
      _int(y / 400) - 32045;
  if (jd < 2299161) {
    jd = dd + _int((153 * m + 2) / 5) + 365 * y + _int(y / 4) - 32083;
  }
  return jd;
}

/* Convert a Julian day number to day/month/year. Parameter jd is an integer */
jdToDate(jd) {
  var a, b, c, d, e, m, day, month, year;
  if (jd > 2299160) { // After 5/10/1582, Gregorian calendar
    a = jd + 32044;
    b = _int((4 * a + 3) / 146097);
    c = a - _int((b * 146097) / 4);
  } else {
    b = 0;
    c = jd + 32082;
  }
  d = _int((4 * c + 3) / 1461);
  e = c - _int((1461 * d) / 4);
  m = _int((5 * e + 2) / 153);
  day = e - _int((153 * m + 2) / 5) + 1;
  month = m + 3 - 12 * _int(m / 10);
  year = b * 100 + d - 4800 + _int(m / 10);
  return [day, month, year];
}

/* Compute the time of the k-th new moon after the new moon of 1/1/1900 13:52 UCT 
 * (measured as the number of days since 1/1/4713 BC noon UCT, e.g., 2451545.125 is 1/1/2000 15:00 UTC).
 * Returns a floating number, e.g., 2415079.9758617813 for k=2 or 2414961.935157746 for k=-2
 * Algorithm from: "Astronomical Algorithms" by Jean Meeus, 1998
 */
newMoon(k) {
  var _t, _t2, _t3, dr, _jd1, _m, _mpr, _f, _c1, deltat, _jdNew;
  _t = k / 1236.85; // Time in Julian centuries from 1900 January 0.5
  _t2 = _t * _t;
  _t3 = _t2 * _t;
  dr = pI / 180;
  _jd1 = 2415020.75933 + 29.53058868 * k + 0.0001178 * _t2 - 0.000000155 * _t3;
  _jd1 = _jd1 + 0.00033 *
      sin((166.56 + 132.87 * _t - 0.009173 * _t2) * dr); // Mean new moon
  _m = 359.2242 + 29.10535608 * k - 0.0000333 * _t2 -
      0.00000347 * _t3; // Sun's mean anomaly
  _mpr = 306.0253 + 385.81691806 * k + 0.0107306 * _t2 +
      0.00001236 * _t3; // Moon's mean anomaly
  _f = 21.2964 + 390.67050646 * k - 0.0016528 * _t2 -
      0.00000239 * _t3; // Moon's argument of latitude
  _c1 = (0.1734 - 0.000393 * _t) * sin(_m * dr) + 0.0021 * sin(2 * dr * _m);
  _c1 = _c1 - 0.4068 * sin(_mpr * dr) + 0.0161 * sin(dr * 2 * _mpr);
  _c1 = _c1 - 0.0004 * sin(dr * 3 * _mpr);
  _c1 = _c1 + 0.0104 * sin(dr * 2 * _f) - 0.0051 * sin(dr * (_m + _mpr));
  _c1 = _c1 - 0.0074 * sin(dr * (_m - _mpr)) + 0.0004 * sin(dr * (2 * _f + _m));
  _c1 = _c1 - 0.0004 * sin(dr * (2 * _f - _m)) - 0.0006 * sin(dr * (2 * _f + _mpr));
  _c1 = _c1 + 0.0010 * sin(dr * (2 * _f - _mpr)) + 0.0005 * sin(dr * (2 * _mpr + _m));
  if (_t < -11) {
    deltat = 0.001 + 0.000839 * _t + 0.0002261 * _t2 - 0.00000845 * _t3 -
        0.000000081 * _t * _t3;
  } else {
    deltat = -0.000278 + 0.000265 * _t + 0.000262 * _t2;
  }
  _jdNew = _jd1 + _c1 - deltat;
  return _jdNew;
}
/* Compute the longitude of the sun at any time. 
 * Parameter: floating number jdn, the number of days since 1/1/4713 BC noon
 * Algorithm from: "Astronomical Algorithms" by Jean Meeus, 1998
 */
_sunLongitude(jdn) {
  var _t, _t2, dr, _m, _l0, _d, _l;
  _t = (jdn - 2451545.0) /
      36525; // Time in Julian centuries from 2000-01-01 12:00:00 GMT
  _t2 = _t * _t;
  dr = pI / 180; // degree to radian
  _m = 357.52910 + 35999.05030 * _t - 0.0001559 * _t2 -
      0.00000048 * _t * _t2; // mean anomaly, degree
  _l0 = 280.46645 + 36000.76983 * _t + 0.0003032 * _t2; // mean longitude, degree
  _d = (1.914600 - 0.004817 * _t - 0.000014 * _t2) * sin(dr * _m);
  _d = _d + (0.019993 - 0.000101 * _t) * sin(dr * 2 * _m) +
      0.000290 * sin(dr * 3 * _m);
  _l = _l0 + _d; // true longitude, degree
  _l = _l * dr;
  _l = _l - pI * 2 * (_int(_l / (pI * 2))); // Normalize to (0, 2*PI)
  return _l;
}

/* Compute sun position at midnight of the day with the given Julian day number. 
 * The time zone if the time difference between local time and UTC: 7.0 for UTC+7:00.
 * The  returns a number between 0 and 11. 
 * From the day after March equinox and the 1st major term after March equinox, 0 is returned. 
 * After that, return 1, 2, 3 ... 
 */
getSunLongitude(dayNumber, timeZone) {
  return _int(_sunLongitude(dayNumber - 0.5 - timeZone / 24) / pI * 6);
}

/* Compute the day of the k-th new moon in the given time zone.
 * The time zone if the time difference between local time and UTC: 7.0 for UTC+7:00
 */
getNewMoonDay(k, timeZone) {
  return _int(newMoon(k) + 0.5 + timeZone / 24);
}

/* Find the day that starts the luner month 11 of the given year for the given time zone */
getLunarMonth11(yy, timeZone) {
  var k, off, nm, sunLong;
  //off = jdFromDate(31, 12, yy) - 2415021.076998695;
  off = jdFromDate(31, 12, yy) - 2415021;
  k = _int(off / 29.530588853);
  nm = getNewMoonDay(k, timeZone);
  sunLong = getSunLongitude(nm, timeZone); // sun longitude at local midnight
  if (sunLong >= 9) {
    nm = getNewMoonDay(k - 1, timeZone);
  }
  return nm;
}

/* Find the index of the leap month after the month starting on the day a11. */
getLeapMonthOffset(a11, timeZone) {
  var k, last, arc, i;
  k = _int((a11 - 2415021.076998695) / 29.530588853 + 0.5);
  last = 0;
  i = 1; // We start with the month following lunar month 11
  arc = getSunLongitude(getNewMoonDay(k + i, timeZone), timeZone);
  do {
    last = arc;
    i++;
    arc = getSunLongitude(getNewMoonDay(k + i, timeZone), timeZone);
  } while (arc != last && i < 14);
  return i - 1;
}

/* Comvert solar date dd/mm/yyyy to the corresponding lunar date */
convertSolar2Lunar(dd, mm, yy, timeZone) {
  var k, dayNumber, monthStart, a11, b11, lunarDay, lunarMonth, lunarYear,
      lunarLeap;
  dayNumber = jdFromDate(dd, mm, yy);
  k = _int((dayNumber - 2415021.076998695) / 29.530588853);
  monthStart = getNewMoonDay(k + 1, timeZone);
  if (monthStart > dayNumber) {
    monthStart = getNewMoonDay(k, timeZone);
  }
  //alert(dayNumber+" -> "+monthStart);
  a11 = getLunarMonth11(yy, timeZone);
  b11 = a11;
  if (a11 >= monthStart) {
    lunarYear = yy;
    a11 = getLunarMonth11(yy - 1, timeZone);
  } else {
    lunarYear = yy + 1;
    b11 = getLunarMonth11(yy + 1, timeZone);
  }
  lunarDay = dayNumber - monthStart + 1;
  if(lunarDay < 10){
    lunarDay = '0$lunarDay';
  }
  var diff = _int((monthStart - a11) / 29);
  lunarLeap = 0;
  lunarMonth = diff + 11;
  if (b11 - a11 > 365) {
    var leapMonthDiff = getLeapMonthOffset(a11, timeZone);
    if (diff >= leapMonthDiff) {
      lunarMonth = diff + 10;
      if (diff == leapMonthDiff) {
        lunarLeap = 1;
      }
    }
  }
  if (lunarMonth > 12) {
    lunarMonth = lunarMonth - 12;
  }
  if (lunarMonth >= 11 && diff < 4) {
    lunarYear -= 1;
  }
  if(lunarMonth < 10){
    lunarMonth = '0$lunarMonth';
  }
  return [lunarDay, lunarMonth, lunarYear, lunarLeap];
}

/* Convert a lunar date to the corresponding solar date */
convertLunar2Solar(lunarDay, lunarMonth, lunarYear, lunarLeap, timeZone) {
  var k, a11, b11, off, leapOff, leapMonth, monthStart;
  if (lunarMonth < 11) {
    a11 = getLunarMonth11(lunarYear - 1, timeZone);
    b11 = getLunarMonth11(lunarYear, timeZone);
  } else {
    a11 = getLunarMonth11(lunarYear, timeZone);
    b11 = getLunarMonth11(lunarYear + 1, timeZone);
  }
  k = _int(0.5 + (a11 - 2415021.076998695) / 29.530588853);
  off = lunarMonth - 11;
  if (off < 0) {
    off += 12;
  }
  if (b11 - a11 > 365) {
    leapOff = getLeapMonthOffset(a11, timeZone);
    leapMonth = leapOff - 2;
    if (leapMonth < 0) {
      leapMonth += 12;
    }
    if (lunarLeap != 0 && lunarMonth != leapMonth) {
      return [0, 0, 0];
    } else if (lunarLeap != 0 || off >= leapOff) {
      off += 1;
    }
  }
  monthStart = getNewMoonDay(k + off, timeZone);
  return jdToDate(monthStart + lunarDay - 1);
}

getCanChiYear(int year) {
  var can = canList[year % 10];
  var chi = chiList[year % 12];
  return '$can $chi';
}

getCanChiMonth(int month, int year) {
  var chi = chiForMonthList[month - 1];
  var indexCan = 0;
  var can = canList[year % 10];

  if (can == "Giáp" || can == "Kỉ") {
    indexCan = 6;
  }
  if (can == "Ất" || can == "Canh") {
    indexCan = 8;
  }
  if (can == "Bính" || can == "Tân") {
    indexCan = 0;
  }
  if (can == "Đinh" || can == "Nhâm") {
    indexCan = 2;
  }
  if (can == "Mậu" || can == "Quý") {
    indexCan = 4;
  }
  return '${canList[(indexCan + month - 1) % 10]} $chi';
}

// getDayName(lunarDate) {
//  if (lunarDate.day == 0) {
//    return "";
//  }
//  var cc = getCanChi(lunarDate);
//  var s = "Ngày " + cc[0] +", tháng "+cc[1] + ", năm " + cc[2];
//  return s;
//}

 getYearCanChi(year) {
  return CAN[(year+6) % 10] + " " + CHI[(year+8) % 12];
}

getCanHour(jdn) {
  return CAN[(jdn - 1) * 2 % 10];
}

 getCanDay(jdn) {
  var dayName;
  dayName = CAN[(jdn + 9) % 10] + " " + CHI[(jdn+1)%12];
  return dayName;
}

jdn(dd, mm, yy) {
  var a = _int((14 - mm) / 12);
  var y = yy+4800-a;
  var m = mm+12*a-3;
  var jd = dd + _int((153*m+2)/5) + 365*y + _int(y/4) - _int(y/100) + _int(y/400) - 32045;
  return jd;
}

getGioHoangDao(jd) {
  var chiOfDay = (jd+1) % 12;
  var gioHD = GIO_HD[chiOfDay % 6]; // same values for Ty' (1) and Ngo. (6), for Suu and Mui etc.
  var ret = "";
  var count = 0;
  for (var i = 0; i < 12; i++) {
    if (gioHD.substring(i, i + 1) == '1') {
      ret += CHI[i];
      ret += ' (${{(i*2+23)%24}}-${{(i*2+1)%24}})';
      if (count++ < 5) ret += ', ';
      if (count == 3) ret += '\n';
    }
  }
  return ret;
}

getTietKhi(jd) {
  return TIETKHI[getSunLongitude(jd + 1, 7.0)];
}

getBeginHour(jdn) {
  return CAN[(jdn - 1) * 2 % 10] + ' ' +  CHI[0];
}
