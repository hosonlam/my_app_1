import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class SharedReferences {
  Future saveData(
      String saveName, String timeName, dynamic jsonResponse) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(saveName, json.encode(jsonResponse));
    var time = (DateTime.now().toUtc().millisecondsSinceEpoch/1000).round();
    prefs.setInt(timeName, time);
  }

  Future getData(String saveName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return json.decode(prefs.getString(saveName));
  }

  Future getTime(String timeName) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      var result = prefs.getInt(timeName);
      return result != null ? result : 0;

  }

  Future clear() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  Future init(String name, String timeName)  async{
      int now =  (DateTime.now().toUtc().millisecondsSinceEpoch/1000).round();
      int prefTime = await SharedReferences().getTime(timeName);
      var time = now - prefTime;
      if(time < 10800){
        return await SharedReferences().getData(name);   
      }else{
        await SharedReferences().clear();
         return null;
      }
     
  }
}
