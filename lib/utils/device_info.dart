import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:package_info/package_info.dart';

class DeviceInfo {
  final bool isTablet;
  final bool isIos;
  bool get isPhone => !isTablet;
  bool get isAndroid => !isIos;
  final bool isEmulator;
  String packageName;
  String packageVersion;
  String packageBuildNumber;
  DeviceInfoPlugin deviceInfo;

  static DeviceInfo instance;
  DeviceInfo(this.isTablet, this.isIos,{this.isEmulator = false});

  static Future init() async {
    var deviceInfo = DeviceInfoPlugin();
    var info = await PackageInfo.fromPlatform();
    if (Platform.isAndroid) {
      instance = processAndroid(await deviceInfo.androidInfo);
    } else {
      instance = processIos(await deviceInfo.iosInfo);
    }
    instance
      ..packageName = info.packageName
      ..packageVersion = info.version
      ..packageBuildNumber = info.buildNumber
      ..deviceInfo = deviceInfo;
  }

  static DeviceInfo processIos(IosDeviceInfo info) {
    var isTablet = info.name.toLowerCase().contains("ipad") ||
                   info.model.toLowerCase().contains("ipad") ||
                   info.utsname.machine.toLowerCase().contains("ipad");
    var isIos = true;
    return DeviceInfo(isTablet, isIos, isEmulator: !info.isPhysicalDevice);
  }

  static DeviceInfo processAndroid(AndroidDeviceInfo info) {
    var data = MediaQueryData.fromWindow(WidgetsBinding.instance.window);
    var isTablet = data.size.shortestSide > 600;
    var isIos = false;
    return DeviceInfo(isTablet, isIos, isEmulator: !info.isPhysicalDevice);
  }

  static Future<bool> checkLowDeviceSpec() async {
    if (Platform.isIOS) {
      var iosDeviceInfo = await instance.deviceInfo.iosInfo;
      var machine = iosDeviceInfo.utsname.machine;
      return machine.contains('iPad4') || machine.contains('iPad5');
    }
    return false;

  }
}