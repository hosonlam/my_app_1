import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class CovidServices {
  Future<Map> get getCovidData async {
    String url = "https://coronavirus-19-api.herokuapp.com/countries/vietnam";
    var response = await http.get(url);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    }
    return null;
  }
}
