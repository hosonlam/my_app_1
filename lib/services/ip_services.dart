import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

import 'package:my_app_1/services/api_key.dart';

class IpServices {

  Future<Map> getExternalIp() async {
    String url = "https://geo.ipify.org/api/v1?apiKey=${ApiKey.externalIpKey}";
    var response = await http.get(url);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse;
    }
    return null;
  }
}
