import 'package:http/http.dart' as http;
import 'package:my_app_1/constant.dart';
import 'package:my_app_1/utils/shared_references.dart';
import 'dart:convert' as convert;
import 'api_key.dart';

class WeatherService {
  Future<Map> getWeatherData(String lat, String long, bool isRefresh) async {
    var saved;
    String url ="http://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$long&appid=${ApiKey.weatherKey}&units=metric&lang=vi";
    if(isRefresh){
      saved = null;
    }else{
      saved = await SharedReferences().init(RefConstant.weatherName,RefConstant.weatherTime);
    }    
    if(saved == null){
      var response = await http.get(url);
      if (response.statusCode == 200) {
        var jsonResponse = convert.jsonDecode(response.body);
        await SharedReferences().saveData(RefConstant.weatherName,RefConstant.weatherTime,jsonResponse);
        return jsonResponse;
      }
    }
      return saved;
  }

  Future<Map> getForecastData(String lat, String long, bool isRefresh) async {
    var saved;
    String url ="https://api.openweathermap.org/data/2.5/onecall?lat=$lat&lon=$long&units=metric&appid=${ApiKey.weatherKey}";
    if(isRefresh){
      saved = null;
    }else{
      saved = await SharedReferences().init(RefConstant.forecastName,RefConstant.forecastTime);
    }
    if(saved == null){
      var response = await http.get(url);
      if (response.statusCode == 200) {
        var jsonResponse = convert.jsonDecode(response.body);
        await SharedReferences().saveData(RefConstant.forecastName,RefConstant.forecastTime,jsonResponse);
        return jsonResponse;
      } 
    }
    return saved;
  }
}
