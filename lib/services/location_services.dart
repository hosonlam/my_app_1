import 'package:android_intent/android_intent.dart';

class LocationServices {
  void openLocationSetting() async {
    final AndroidIntent intent = new AndroidIntent(
      action: 'android.settings.LOCATION_SOURCE_SETTINGS',
    );
    await intent.launch();
  }

  void openLocationPermisson() async {
    final AndroidIntent intent = new AndroidIntent(
      action: 'android.',
    );
    await intent.launch();
  }
}
