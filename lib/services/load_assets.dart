import 'dart:convert';

import 'package:flutter/services.dart';

class LoadAssets{
  Future<void> loadHtmlFromAssets(String fileName, controller) async{
    String fileText = await rootBundle.loadString(fileName);
    controller.loadUrl(Uri.dataFromString(fileText,mimeType: 'text/html',encoding: Encoding.getByName('utf-8')).toString());
  }
}