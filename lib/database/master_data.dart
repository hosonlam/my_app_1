import 'dart:io';

import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class MasterData {
  static Database db;

  Future open() async {
    var dbDir = await getExternalStorageDirectory();
    var dbPath = join(dbDir.path, "myApp.db");
    var exist = await databaseExists(dbPath);
    if (exist) {
      db = await openDatabase(dbPath);
    } else {
      await deleteDatabase(dbPath);
      ByteData data = await rootBundle.load("assets/database/myApp.db");
      List<int> bytes = data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      await File(dbPath).writeAsBytes(bytes);
      db = await openDatabase(dbPath);
    }
    return db;
  }
}
