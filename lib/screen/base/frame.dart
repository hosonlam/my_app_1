import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_app_1/constant.dart';
import 'package:my_app_1/screen/calculator.dart';
import 'package:my_app_1/screen/debit.dart';
import 'package:my_app_1/screen/electric.dart';
import 'package:my_app_1/screen/home.dart';
import 'package:my_app_1/screen/info/info.dart';
import 'package:my_app_1/screen/water.dart';
import 'package:my_app_1/utils/static.dart';
import 'package:my_app_1/widget/bottom_navigator.dart';
import 'package:my_app_1/widget/calendar.dart';
import 'package:my_app_1/widget/swipe_gesture.dart';
import 'package:my_app_1/widget/clock.dart';
import 'package:my_app_1/widget/drawer.dart';

class Frame extends StatefulWidget {
  Frame({Key key}) : super(key: key);

  @override
  _FrameState createState() => _FrameState();
}

class _FrameState extends State<Frame> with TickerProviderStateMixin {
  PageController _pageController = PageController();
  GlobalKey _bottomNavigationKey = GlobalKey();
  bool isDrag = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    var _height = UI.height(context);
    return Scaffold(
      primary: false,
      resizeToAvoidBottomPadding: false,
      drawer: DrawerCustom(bottomNavigationKey: _bottomNavigationKey),
      appBar: AppBar(
        primary: false,
        backgroundColor: Season.getColors[StaticVariable.season],
        bottom: isDrag
            ? PreferredSize(
                preferredSize: Size.fromHeight(_height * 0.12),
                child: SwipeDetector(
                  onSwipeUp: () {
                    setState(() {
                      isDrag = false;
                    });
                  },
                  child: AnimatedContainer(
                    height: _height * 0.12,
                    duration: Duration(seconds: 2),
                    curve: Curves.fastOutSlowIn,
                    child: Calendar(
                      height: _height * 0.12,
                    ),
                  ),
                ))
            : PreferredSize(preferredSize: Size.zero, child: Container()),
        centerTitle: true,
        title: SwipeDetector(
            onSwipeDown: () {
              setState(() {
                isDrag = true;
              });
            },
            onTap: () {
              setState(() {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CalculatorScreen()));
              });
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(),
                Container(child: Text(tr("app_title"))),
                Center(child: Clock())
              ],
            )),
      ),
      body: PageView(
        controller: _pageController,
        children: <Widget>[
          InfoScreen(),
          ElectricScreen(),
          HomeScreen(),
          WaterScreen(),
          DebitScreen()
        ],
        onPageChanged: (index) {
          final CurvedNavigationBarState navBarState =
              _bottomNavigationKey.currentState;
          navBarState.setPage(index);
        },
      ),
      bottomNavigationBar: BottomNavigator(
          bottomNavigationKey: _bottomNavigationKey,
          pageController: _pageController),
    );
  }
}
