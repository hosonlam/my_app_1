import 'package:clipboard_manager/clipboard_manager.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:math_expressions/math_expressions.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:my_app_1/utils/flutter_money_formatter.dart';
import 'package:my_app_1/utils/static.dart';
import 'package:my_app_1/constant.dart';

import 'base/frame.dart';

class CalculatorScreen extends StatefulWidget {
  @override
  State createState() => _CalculatorScreenState();
}

class _CalculatorScreenState extends State<CalculatorScreen> {
  //Load the last saved value using SharedPreferences
  TextEditingController textControllerInput;
  TextEditingController textControllerResult;

  @override
  void initState() {
    super.initState();
    // Start listening to changes
    textControllerInput = TextEditingController();
    textControllerResult = TextEditingController();
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is removed from the Widget tree
    textControllerInput.dispose();
    textControllerResult.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
        resizeToAvoidBottomPadding: true,
        //APPBAR
        appBar: AppBar(
          backgroundColor: Season.getColors[StaticVariable.season],
          leading: IconButton(icon: Icon(Icons.arrow_back_ios),onPressed: () {
            setState(() {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Frame()));
              });
          },),
          title: Text(tr("calculator_title")),
          elevation: 0,
        ),
        //BODY
        body: Container(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Container(
                  padding: EdgeInsets.all(10),
                  child: TextField(
                    maxLines: 3,
                    decoration: InputDecoration.collapsed(hintText: "0"),
                    style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.w400,
                      //                          color: Colors.black87
                    ),      
                    textAlign: TextAlign.right,
                    controller: textControllerInput,
                    onTap: () =>
                        FocusScope.of(context).requestFocus(FocusNode()),
                  )),
            ),
            Expanded(
              flex: 2,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: TextField(
                          maxLines: 3,
                          decoration: InputDecoration.collapsed(
                              hintText: tr("calculator_sum"),
                              fillColor: Colors.deepPurpleAccent),
                          textInputAction: TextInputAction.none,
                          keyboardType: TextInputType.number,
                          style: TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.w400,
                              color: Colors.green),
                          textAlign: TextAlign.right,
                          controller: textControllerResult,
                          readOnly: true,
                          onTap: () {
                            FocusScope.of(context).requestFocus(FocusNode());
                            ClipboardManager.copyToClipBoard(
                                    textControllerResult.text)
                                .then((result) {
                              Fluttertoast.showToast(
                                  msg: tr("calculator_message"),
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  backgroundColor: Colors.blueAccent,
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                            });
                          },
                        )),
                  ),
                  Container(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      alignment: Alignment.centerRight,
                      child: IconButton(
                          icon: Icon(
                            Icons.cancel,
                            size: 50,
                          ),
                          color: Colors.green,
                          onPressed: () {
                            setState(() {
                              textControllerInput.text = "";
                              textControllerResult.text = "";
                            });
                          })),
                ],
              ),
            ),
            SizedBox(height: 50),
            keypadContainer(),
          ],
        )));
  }

  Row buttonsRow1Container() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Expanded(
          child: FlatButton(
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: Text("7",
                style: TextStyle(fontSize: 50, color: Colors.blueGrey)),
            onPressed: () => setState(() {
              textControllerInput.text = textControllerInput.text + "7";
              _calculate();
            }),
          ),
        ),
        Expanded(
          child: FlatButton(
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: Text("8",
                style: TextStyle(fontSize: 50, color: Colors.blueGrey)),
            onPressed: () => setState(() {
              textControllerInput.text = textControllerInput.text + "8";
              _calculate();
            }),
          ),
        ),
        Expanded(
          child: FlatButton(
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: Text("9",
                style: TextStyle(fontSize: 50, color: Colors.blueGrey)),
            onPressed: () => setState(() {
              textControllerInput.text = textControllerInput.text + "9";
              _calculate();
            }),
          ),
        ),
        FlatButton(
          padding: const EdgeInsets.symmetric(vertical: 30),
          child:
              Text("/", style: TextStyle(fontSize: 50, color: Colors.blueGrey)),
          color: Colors.green[50],
          onPressed: () => setState(() {
            textControllerInput.text = textControllerInput.text + "/";
          }),
        ),
      ],
    );
  }

  //This is the second Row of keys
  Row buttonsRow2Container() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Expanded(
          child: FlatButton(
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: Text("4",
                style: TextStyle(fontSize: 50, color: Colors.blueGrey)),
            onPressed: () => setState(() {
              textControllerInput.text = textControllerInput.text + "4";
              _calculate();
            }),
          ),
        ),
        Expanded(
          child: FlatButton(
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: Text("5",
                style: TextStyle(fontSize: 50, color: Colors.blueGrey)),
            onPressed: () => setState(() {
              textControllerInput.text = textControllerInput.text + "5";
              _calculate();
            }),
          ),
        ),
        Expanded(
          child: FlatButton(
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: Text("6",
                style: TextStyle(fontSize: 50, color: Colors.blueGrey)),
            onPressed: () => setState(() {
              textControllerInput.text = textControllerInput.text + "6";
              _calculate();
            }),
          ),
        ),
        FlatButton(
          padding: const EdgeInsets.symmetric(vertical: 30),
          child:
              Text("x", style: TextStyle(fontSize: 50, color: Colors.blueGrey)),
          color: Colors.green[50],
          onPressed: () => setState(() {
            textControllerInput.text = textControllerInput.text + "*";
          }),
        ),
      ],
    );
  }

  //This is the third Row of keys
  Row buttonsRow3Container() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Expanded(
          child: FlatButton(
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: Text("1",
                style: TextStyle(fontSize: 50, color: Colors.blueGrey)),
            onPressed: () => setState(() {
              textControllerInput.text = textControllerInput.text + "1";
              _calculate();
            }),
          ),
        ),
        Expanded(
          child: FlatButton(
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: Text("2",
                style: TextStyle(fontSize: 50, color: Colors.blueGrey)),
            onPressed: () => setState(() {
              textControllerInput.text = textControllerInput.text + "2";
              _calculate();
            }),
          ),
        ),
        Expanded(
          child: FlatButton(
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: Text("3",
                style: TextStyle(fontSize: 50, color: Colors.blueGrey)),
            onPressed: () => setState(() {
              textControllerInput.text = textControllerInput.text + "3";
              _calculate();
            }),
          ),
        ),
        FlatButton(
          padding: const EdgeInsets.symmetric(vertical: 30),
          child:
              Text("-", style: TextStyle(fontSize: 50, color: Colors.blueGrey)),
          color: Colors.green[50],
          onPressed: () => setState(() {
            textControllerInput.text = textControllerInput.text + "-";
          }),
        ),
      ],
    );
  }

  //This is the second Row of keys
  Row buttonsRow4Container() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Expanded(
          child: FlatButton(
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: Text(".",
                style: TextStyle(fontSize: 50, color: Colors.blueGrey)),
            onPressed: () => setState(() {
              textControllerInput.text = textControllerInput.text + ".";
            }),
          ),
        ),
        Expanded(
          child: FlatButton(
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: Text("0",
                style: TextStyle(fontSize: 50, color: Colors.blueGrey)),
            onPressed: () => setState(() {
              textControllerInput.text = textControllerInput.text + "0";
              _calculate();
            }),
          ),
        ),
        Expanded(
          child: FlatButton(
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: Icon(Icons.backspace, size: 50, color: Colors.blueGrey),
            onPressed: () {
              textControllerInput.text = (textControllerInput.text.length > 0)
                  ? (textControllerInput.text
                      .substring(0, textControllerInput.text.length - 1))
                  : "";
              _calculate();
            },
          ),
        ),
        FlatButton(
          padding: const EdgeInsets.symmetric(vertical: 30),
          child:
              Text("+", style: TextStyle(fontSize: 50, color: Colors.blueGrey)),
          color: Colors.green[50],
          onPressed: () => setState(() {
            textControllerInput.text = textControllerInput.text + "+";
          }),
        )
      ],
    );
  }

  //This returns a Column for the keypad
  Column keypadContainer() {
    return Column(
      children: <Widget>[
        buttonsRow1Container(),
        buttonsRow2Container(),
        buttonsRow3Container(),
        buttonsRow4Container(),
      ],
    );
  }

  Future<void> _calculate() async {
    await Future.delayed(Duration(milliseconds: 500));
    //Calculate everything here
    // Parse expression:
    Parser p = Parser();
    // Bind variables:
    ContextModel cm = ContextModel();
    Expression exp = p.parse(textControllerInput.text);
    FlutterMoneyFormatter fmf = FlutterMoneyFormatter(amount: exp.evaluate(EvaluationType.REAL, cm));
    textControllerResult.text = fmf.output.withoutFractionDigits;
        
  }
}
