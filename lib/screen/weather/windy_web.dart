import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:my_app_1/services/load_assets.dart';

class WindyScren extends StatefulWidget {
  @override
  _WindyScrenState createState() => _WindyScrenState();
}

class _WindyScrenState extends State<WindyScren> {
  WebViewController _controller;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      body: SafeArea(
          child: WebView(
        javascriptMode: JavascriptMode.unrestricted,
        onWebViewCreated: (WebViewController webViewController) async {
          _controller = webViewController;
          await LoadAssets()
              .loadHtmlFromAssets('assets/html/windy.html', _controller);
        },
      )),
    );
  }
}
