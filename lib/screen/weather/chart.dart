import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:my_app_1/model/weather.dart';
import 'package:my_app_1/utils/convert_timestamp.dart';

class WeatherChart extends StatefulWidget {
  final  WeatherForecast model;

  const WeatherChart({Key key, this.model}) : super(key: key);
  
  @override
  State createState() => _WeatherChartState();
}

class _WeatherChartState extends State<WeatherChart> {
  int counter = 0;
  List<FlSpot> spots = [];
  
@override
  void initState() {
    super.initState();
    widget.model.hourly.take(11).forEach((e) {
      spots.add(FlSpot(counter.ceilToDouble(),e.temp.toDouble()));
      counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return LineChart(
        LineChartData(
          backgroundColor: Colors.transparent,
          borderData: FlBorderData(show: false),
          gridData: FlGridData(show: false),
          titlesData: FlTitlesData(
              show: true,
              bottomTitles: SideTitles(
                  textStyle: TextStyle(color: Colors.white),
                  showTitles: true,
                  reservedSize:0,
                  interval: 2,
                  margin: 30,
                  getTitles: (val) {
                    switch (val.toInt()) {
                      case 0:
                        return ConvertTimestamp.convert(widget.model.hourly[0].dt,widget.model.timezoneOffset).hour.toString();
                      case 1:
                        return ConvertTimestamp.convert(widget.model.hourly[1].dt,widget.model.timezoneOffset).hour.toString();
                      case 2: 
                        return ConvertTimestamp.convert(widget.model.hourly[2].dt,widget.model.timezoneOffset).hour.toString();
                      case 3:
                        return ConvertTimestamp.convert(widget.model.hourly[3].dt,widget.model.timezoneOffset).hour.toString();
                      case 4:
                        return ConvertTimestamp.convert(widget.model.hourly[4].dt,widget.model.timezoneOffset).hour.toString();
                      case 5:
                        return ConvertTimestamp.convert(widget.model.hourly[5].dt,widget.model.timezoneOffset).hour.toString();
                      case 6: 
                        return ConvertTimestamp.convert(widget.model.hourly[6].dt,widget.model.timezoneOffset).hour.toString();
                      case 7:
                        return ConvertTimestamp.convert(widget.model.hourly[7].dt,widget.model.timezoneOffset).hour.toString();
                      case 8:
                        return ConvertTimestamp.convert(widget.model.hourly[8].dt,widget.model.timezoneOffset).hour.toString();
                      case 9:
                        return ConvertTimestamp.convert(widget.model.hourly[9].dt,widget.model.timezoneOffset).hour.toString();
                      case 10:
                        return ConvertTimestamp.convert(widget.model.hourly[10].dt,widget.model.timezoneOffset).hour.toString();
                    }
                    return '';
                  }),
              leftTitles: SideTitles(
                getTitles: (value) {
                  return value.toStringAsFixed(0);
                },
                showTitles: true,
                margin: 30,
                reservedSize:1,
                textStyle: TextStyle(
                  color: Colors.white),
              ),
          ),
          lineTouchData: LineTouchData( enabled: false),
          lineBarsData: [
            LineChartBarData(
              spots: spots,
              colors: [Colors.white],
              isCurved: true,
              barWidth: 1,
              dashArray: [4],
              belowBarData: BarAreaData(show: false),
              dotData: FlDotData(show: true),
            ),
          ],
        ),
      );
  }
}

