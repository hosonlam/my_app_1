import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:my_app_1/bloc/weather_bloc/weather_bloc.dart';
import 'package:my_app_1/screen/base/frame.dart';
import 'package:my_app_1/screen/weather/chart.dart';
import 'package:my_app_1/screen/weather/windy_web.dart';
import 'package:my_app_1/utils/convert_timestamp.dart';
import 'package:my_app_1/widget/loading.dart';
import 'package:my_app_1/widget/popup_dialog.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class WeatherScreen extends StatefulWidget {
  @override
  _WeatherScreenState createState() => _WeatherScreenState();
}

class _WeatherScreenState extends State<WeatherScreen> {
  WeatherBloc bloc = WeatherBloc();
  RefreshController _refreshController = RefreshController(initialRefresh: false);
  
  @override
  void initState() {
    super.initState();
    bloc.add(LoadDataEvent());
  }

  @override
  void dispose() {
    super.dispose();
    bloc.close();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return BlocProvider(
        create: (context) => bloc,
        child: BlocListener<WeatherBloc, WeatherState>(listener:
            (context, state) {
          if (state is ErrorWeatherState) {
            if (state.message != null) {
              DialogUtilities.showError(context, state.message, isLock: true,
                  action: () {
                setState(() {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Frame()));
                  openAppSettings();
                });
              });
            }
            if(state.message == null){
              Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Frame()));
            }
          }
        }, child:
            BlocBuilder<WeatherBloc, WeatherState>(builder: (context, state) {
          if (state is LoadedWeatherState) {
            return getMainWidget(state);
          }
          return Container(
            color: Colors.white,
          );
        })));
  }

  Widget getMainWidget(LoadedWeatherState state) {
    var data = state.data;
    var dataF = state.dataF;
    if (state.loading != null && state.loading == true) {
      return LoadingWidget();
    } else {
      return Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            actions: <Widget>[
              IconButton(
              icon: Icon(
                Icons.map,
                color: Colors.white,
              ),
              onPressed: () {
                setState(() {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => WindyScren()));
                });
              },
            ),
            ],
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
              ),
              onPressed: () {
                setState(() {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Frame()));
                });
              },
            ),
            title: Text(
              "${data?.name}",
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
            centerTitle: true,
            elevation: 0,
            backgroundColor: Colors.transparent,
          ),
          body: SmartRefresher(
            controller: _refreshController,
            onRefresh: _onRefresh,
            onLoading: _onLoading,
            enablePullDown: true,
            header: WaterDropHeader(),
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage( 
                  colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.4), BlendMode.darken),
                    image: AssetImage(state.path),
                    fit: BoxFit.cover),
              ),
              child: SafeArea(
                child: Container(
                  width: double.infinity,
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 20),
                      Text(
                          '${data.main?.tempMax} \u2103 - '
                          '${data.main?.tempMin} \u2103',
                          style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              fontWeight: FontWeight.w500)),
                      SizedBox(height: 10),
                      Text('${data.main?.temp}\u00B0',
                          style: TextStyle(
                              fontSize: 200,
                              color: Colors.white,
                              fontWeight: FontWeight.w500)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          _icon(
                              data: '${data.wind?.speed} m/s',
                              icon: 'assets/icons/wind.svg',
                              title: tr("weather_wind")),
                          SizedBox(width: 30),
                          _icon(
                              data: '${data.main?.humidity} %',
                              icon: 'assets/icons/humidity.svg',
                              title: tr("weather_humidity")),
                          SizedBox(width: 30),
                          _icon(
                              data:
                                  '${ConvertTimestamp.convert(data.sys?.sunrise,DateTime.now().timeZoneOffset.inSeconds).hour} giờ',
                              icon: 'assets/icons/sunrise.svg',
                              title: tr("weather_sunraise")),
                          SizedBox(width: 30),
                          _icon(
                              data:
                                  '${ConvertTimestamp.convert(data.sys?.sunset,DateTime.now().timeZoneOffset.inSeconds).hour} giờ',
                              icon: 'assets/icons/sunset.svg',
                              title: tr("weather_sunset")),
                        ],
                      ),
                      SizedBox(height: 50),
                      Expanded(child: WeatherChart(model:dataF)),
                      SizedBox(height: 50),
                    ],
                  ),
                ),
              ),
            ),
          ));
    }
  }

  Widget _icon({String icon, String title, String data}) {
    return Column(
      children: <Widget>[
        SvgPicture.asset(
          '$icon',
          width: 30,
          height: 30,
        ),
        SizedBox(height: 4),
        Text('$title',
            style: TextStyle(
                fontSize: 14,
                color: Colors.white,
                fontWeight: FontWeight.w500)),
        SizedBox(
          height: 4,
        ),
        Text('$data',
            style: TextStyle(
                fontSize: 14, color: Colors.white, fontWeight: FontWeight.w500))
      ],
    );
  }

    void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    if (mounted)
      setState(() {
        bloc.add(LoadDataEvent(isRefresh: true));
      });
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    await Future.delayed(Duration(milliseconds: 1000));
    _refreshController.loadComplete();
  }
}
