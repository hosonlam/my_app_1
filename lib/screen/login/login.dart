import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_app_1/bloc/login_bloc/login_bloc.dart';
import 'package:my_app_1/screen/base/frame.dart';
import 'package:my_app_1/widget/popup_dialog.dart';
import 'package:local_auth/local_auth.dart';
import 'package:my_app_1/constant.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreen createState() => _LoginScreen();
}

class _LoginScreen extends State<LoginScreen> {
  final LocalAuthentication auth = LocalAuthentication();
  final _formKey = GlobalKey<FormState>();
  final _color = Colors.white;
  LoginBloc bloc = LoginBloc();
  String id, password;

  @override
  void initState() {
    bloc.add(LoadDataEvent());
    super.initState();
  }

  @override
  void dispose() {
    bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return BlocProvider(
        create: (context) => bloc,
        child: BlocListener<LoginBloc, LoginState>(listener: (context, state) {
          if (state is LoadedLoginState) {
            if (state.message != null && state.login != true) {
              DialogUtilities.showError(context, state.message, isLock: true);
            } else if (state.login == true) {
              setState(() {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Frame()));
              });
            }
          }
        }, child: BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
          if (state is LoadedLoginState) {
            return getMainWidget();
          } else {
            return Container();
          }
        })));
  }

  Widget getMainWidget() {
    var _width = UI.width(context);
    var _height = UI.height(context);
    return Scaffold(
      body: Container(
        width: _width,
        height: _height,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
              Colors.blue[700],
              Colors.blue[500],
              Colors.blue[400]
            ])),
        child: Form(
          key: _formKey,
          child: Align(
            alignment: Alignment.center,
            child: Container(
              decoration: BoxDecoration(
                  color: _color,
                  borderRadius: BorderRadius.circular(18),
                  border: Border.all(color: Colors.transparent)),
              padding: const EdgeInsets.symmetric(horizontal: 20),
              width: _width * 0.85,
              height: _height * 0.6,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    "assets/images/flutter_logo.png",
                    fit: BoxFit.cover,
                    scale: 1,
                    width: _width * 0.45,
                  ),
                  SizedBox(height: 40),
                  TextFormField(
                    autocorrect: false,
                    enableSuggestions: false,
                    decoration: InputDecoration(
                        hintText: tr("login_id"),
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: _color),
                            borderRadius: BorderRadius.circular(16)),
                        fillColor: Colors.white,
                        filled: true,
                        prefixIcon: Icon(Icons.people)),
                    onFieldSubmitted: (value) {
                      id = value;
                    },
                    validator: (value) {
                      id = value;
                      return;
                    },
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    autocorrect: false,
                    enableSuggestions: false,
                    obscureText: true,
                    decoration: InputDecoration(
                        hintText: tr("login_password"),
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: _color, width: 1.0),
                            borderRadius: BorderRadius.circular(16)),
                        fillColor: Colors.white,
                        filled: true,
                        prefixIcon: Icon(Icons.lock)),
                    onFieldSubmitted: (value) {
                      password = value;
                    },
                    validator: (value) {
                      password = value;
                      return;
                    },
                  ),
                  SizedBox(height: 30),
                  Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.lightBlueAccent,
                        borderRadius: BorderRadius.circular(24)),
                    child: FlatButton(
                      onPressed: () {
                        bloc.add(ValidatedEvent(id, password));
                      },
                      child: Text(
                        tr("login_sign_in_button"),
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.orangeAccent,
                        borderRadius: BorderRadius.circular(24)),
                    child: FlatButton(
                      onPressed: () async {
                        bool canCheckBiometrics = await auth.canCheckBiometrics;
                        if (canCheckBiometrics) {
                          bool didAuthenticate =
                              await auth.authenticateWithBiometrics(
                                  localizedReason: tr("login_auth_message"));
                          if (didAuthenticate) {
                            setState(() {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Frame()));
                            });
                          } else {
                            DialogUtilities.showError(
                                context, tr("login_auth_fail"),
                                isLock: true);
                            return;
                          }
                        } else {
                          DialogUtilities.showError(
                              context, tr("login_auth_device_not_support"),
                              isLock: true);
                          return;
                        }
                      },
                      child: Text(
                        tr("login_auth_title"),
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
