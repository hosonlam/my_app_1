import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_app_1/bloc/account_bloc/account_bloc.dart';
import 'package:my_app_1/model/account_store.dart';
import 'package:my_app_1/screen/account_store/account.dart';

class AccountDetail extends StatefulWidget {
  final AccountStoreModel model;
  final bool isEdit;
  final AccountBloc bloc;
  const AccountDetail({Key key, this.model, this.isEdit, this.bloc})
      : super(key: key);

  @override
  _AccountDetailState createState() => _AccountDetailState();
}

class _AccountDetailState extends State<AccountDetail> {
  bool enableField = false;
  bool _obscureText = true;
  FocusNode titleFocus = FocusNode();
  FocusNode usernameFocus = FocusNode();
  FocusNode passwordFocus = FocusNode();
  FocusNode notesFocus = FocusNode();
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    if (widget.isEdit) {
      widget.bloc.add(LoadDetailEvent(widget.model));
    } else {
      widget.bloc.add(NewDataEvent());
    }
  }

  @override
  void dispose() {
    super.dispose();
    widget.bloc.close();
  }

  void _toggleSaveEdit() {
    setState(() {
      enableField = !enableField;
    });
  }

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return BlocProvider(
        create: (context) => widget.bloc,
        child: BlocListener<AccountBloc, AccountState>(listener:
            (context, state) {
          if (state is LoadedDetailState) {}
        }, child:
            BlocBuilder<AccountBloc, AccountState>(builder: (context, state) {
          if (state is LoadedDetailState) {
            var model = state.data;
            return Scaffold(
                key: _scaffoldKey,
                appBar: AppBar(
                    leading: IconButton(
                      icon: Icon(Icons.arrow_back_ios),
                      onPressed: () {
                        setState(() {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AccountScreen()));
                        });
                      },
                    ),
                    backgroundColor: Colors.black,
                    centerTitle: true,
                    title: Text(state.data?.title ?? tr("contact_add")),
                    actions: _action(state)),
                body: _mainWidget(model));
          }
          return Container();
        })));
  }

  List<Widget> _action(LoadedDetailState state) {
    return <Widget>[
      Visibility(
        visible: !enableField,
        child: IconButton(
          icon: Icon(Icons.edit),
          onPressed: () {
            setState(() {
              _toggleSaveEdit();
            });
          },
        ),
      ),
      Visibility(
        visible: enableField,
        child: IconButton(
          icon: Icon(Icons.save),
          onPressed: () {
            setState(() {
              {
                if (_formKey.currentState.validate()) {
                  if (widget.isEdit) {
                    widget.bloc.add(SaveDataEvent(model: state.data));
                  } else {
                    widget.bloc.add(AddDataEvent(model: state.data));
                  }
                }
                _scaffoldKey.currentState.showSnackBar(
                  SnackBar(
                    content: Text(tr("account_saved")),
                  ),
                );
              }
              _toggleSaveEdit();
            });
          },
        ),
      )
    ];
  }

  Widget _mainWidget(AccountStoreModel model) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      color: Colors.black,
      child: Column(
        children: <Widget>[
          Expanded(
            child: Form(
              key: _formKey,
              child: ListView(
                children: <Widget>[
                  TextFormField(
                    enabled: enableField,
                    decoration: InputDecoration(
                        hintText: tr("account_name_hint"),
                        labelText: tr("account_name_title"),
                        hintStyle: TextStyle(color: Colors.white54),
                        labelStyle: TextStyle(color: Colors.white54),
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        prefixIcon: Icon(Icons.web, color: Colors.white54)),
                    initialValue: model?.title ?? "",
                    style: TextStyle(color: Colors.white),
                    onFieldSubmitted: (val) {
                      setState(() {
                        model.title = val;
                        widget.bloc.add(DetailChangeEvent(model));
                        titleFocus.requestFocus();
                      });
                    },
                    validator: (value) {
                      if (value.isEmpty) {
                        return tr("contact_validate_message");
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 12),
                  TextFormField(
                    enabled: enableField,
                    decoration: InputDecoration(
                        hintText: tr("account_username_hint"),
                        labelText: tr("account_username_title"),
                        hintStyle: TextStyle(color: Colors.white54),
                        labelStyle: TextStyle(color: Colors.white54),
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        prefixIcon:
                            Icon(Icons.account_box, color: Colors.white54)),
                    initialValue: model?.username ?? "",
                    style: TextStyle(color: Colors.white),
                    onFieldSubmitted: (val) {
                      setState(() {
                        model.username = val;
                        widget.bloc.add(DetailChangeEvent(model));
                        usernameFocus.requestFocus();
                      });
                    },
                    validator: (value) {
                      if (value.isEmpty) {
                        return tr("contact_validate_message");
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 12),
                  TextFormField(
                    enabled: enableField,
                    decoration: InputDecoration(
                        labelText: tr("account_password_title"),
                        labelStyle: TextStyle(color: Colors.white54),
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        prefixIcon: Icon(Icons.lock, color: Colors.white54)),
                    initialValue: model?.password ?? "",
                    style: TextStyle(color: Colors.white),
                    obscureText: _obscureText,
                    onFieldSubmitted: (val) {
                      setState(() {
                        model.password = val;
                        widget.bloc.add(DetailChangeEvent(model));
                        usernameFocus.requestFocus();
                      });
                    },
                    validator: (value) {
                      if (value.isEmpty) {
                        return tr("contact_validate_message");
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 12),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      FlatButton(
                        onPressed: _toggle,
                        child: Text(
                          _obscureText
                              ? tr("account_password_show")
                              : tr("account_password_hide"),
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 18),
                        ),
                      ),
                      FlatButton(
                        onPressed: () {
                          Clipboard.setData(
                              new ClipboardData(text: model.password));
                          _scaffoldKey.currentState.showSnackBar(
                            SnackBar(
                              content: Text(tr("account_copied")),
                            ),
                          );
                        },
                        child: Text(
                          tr("account_password_copy"),
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 18),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 12),
                  TextFormField(
                    enabled: enableField,
                    decoration: InputDecoration(
                        hintText: tr("account_note_hint"),
                        labelText: tr("account_note_title"),
                        hintStyle: TextStyle(color: Colors.white54),
                        labelStyle: TextStyle(color: Colors.white54),
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        prefixIcon: Icon(Icons.note, color: Colors.white54)),
                    initialValue: model?.note ?? "",
                    style: TextStyle(color: Colors.white),
                    onFieldSubmitted: (val) {
                      setState(() {
                        model.note = val;
                        widget.bloc.add(DetailChangeEvent(model));
                        usernameFocus.requestFocus();
                      });
                    },
                    validator: (value) {
                      if (value.isEmpty) {
                        return tr("contact_validate_message");
                      }
                      return null;
                    },
                  ),
                ],
              ),
            ),
          ),
          InkWell(
            onTap: () {
              setState(() {
                widget.bloc.add(DeleteEvent(model));
                Navigator.pop(context);
              });
            },
            child: Container(
              width: 200,
              height: 50,
              decoration: BoxDecoration(
                  color: Colors.red, borderRadius: BorderRadius.circular(20)),
              child: Center(
                child: Text(
                  tr("contact_del"),
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
              ),
            ),
          ),
          SizedBox(height: 10)
        ],
      ),
    );
  }
}
