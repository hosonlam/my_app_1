import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_app_1/bloc/account_bloc/account_bloc.dart';
import 'package:my_app_1/constant.dart';
import 'package:my_app_1/screen/account_store/account_detail.dart';
import 'package:my_app_1/screen/base/frame.dart';
import 'package:my_app_1/utils/icon.dart';
import 'package:my_app_1/utils/static.dart';

class AccountScreen extends StatefulWidget {
  @override
  _AccountScreen createState() => _AccountScreen();
}

class _AccountScreen extends State<AccountScreen> {
  AccountBloc bloc = AccountBloc();

  @override
  void initState() {
    super.initState();
    bloc.add(LoadDataEvent());
  }

  @override
  void dispose() {
    super.dispose();
    bloc.close();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            setState(() {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Frame()));
            });
          },
        ),
        backgroundColor: Colors.black,
        centerTitle: true,
        title: Text(
          tr("account_manager_title"),
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
        ),
      ),
      body: BlocProvider(
          create: (context) => bloc,
          child: BlocListener<AccountBloc, AccountState>(listener:
              (context, state) {
            if (state is LoadedAccountState) {}
          }, child:
              BlocBuilder<AccountBloc, AccountState>(builder: (context, state) {
            if (state is LoadedAccountState) {
              return _mainWidget(state);
            }
            return Container();
          }))),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Season.getColors[StaticVariable.season],
        onPressed: () {
          setState(() {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        AccountDetail(isEdit: false, bloc: bloc)));
          });
        },
        child: Icon(MyIcon.add),
      ),
    );
  }

  Widget _mainWidget(LoadedAccountState state) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      color: Colors.black,
      child: ListView.builder(
          itemCount: state.datas?.length ?? 0,
          itemBuilder: (BuildContext context, int index) {
            return _listItem(state, index);
          }),
    );
  }

  Widget _listItem(LoadedAccountState state, int index) {
    var model = state.datas[index];
    return Container(
      child: ListTile(
        leading: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                color: Colors.white,
              ),
              height: 50,
              width: 50,
              child: Icon(
                Icons.accessibility,
                color: Colors.black,
              ),
            ),
          ],
        ),
        title: Text(
          "${model.title}",
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w400,fontSize: 18),
        ),
        subtitle: Text(
          "${model.username}",
          style: TextStyle(
              color: Colors.white60, fontSize: 14, fontStyle: FontStyle.italic),
        ),
        onLongPress: () {},
        onTap: () {
          setState(() {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => AccountDetail(
                          model: model,
                          bloc: bloc,
                          isEdit: true,
                        )));
          });
        },
      ),
    );
  }
}
