import 'package:easy_localization/easy_localization.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_app_1/bloc/fund_bloc/fund_bloc.dart';
import 'package:my_app_1/constant.dart';
import 'package:my_app_1/utils/icon.dart';
import 'package:my_app_1/utils/static.dart';

class FundScreen extends StatefulWidget {
  @override
  _FundScreen createState() => _FundScreen();
}

class _FundScreen extends State<FundScreen> {
  FundBloc bloc = FundBloc();
  List<FlSpot> spots = [];
  int counter = 0;

  @override
  void initState() {
    super.initState();
    bloc.add(LoadDataEvent());
    bloc.chartData.forEach((e) {
      spots.add(FlSpot(counter.ceilToDouble(), e.amount.toDouble()));
      counter++;
    });
  }

  @override
  void dispose() {
    super.dispose();
    bloc.close();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Season.getColors[StaticVariable.season],
        onPressed: () {
          setState(() {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => Container()));
          });
        },
        icon: Icon(MyIcon.add),
        label: Text(
          tr("contact_add"),
        ),
      ),
      body: BlocProvider(
          create: (context) => bloc,
          child: BlocListener<FundBloc, FundState>(listener: (context, state) {
            if (state is LoadedFundState) {}
          }, child: BlocBuilder<FundBloc, FundState>(builder: (context, state) {
            if (state is LoadedFundState) {
              return mainWidget(state);
            }
            return Container();
          }))),
    );
  }

  Widget mainWidget(LoadedFundState state) {
    var _height = UI.height(context);
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Container(
            height: _height * 0.3,
            decoration: BoxDecoration(
                color: Season.getColors[StaticVariable.season],
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(12),
                    bottomRight: Radius.circular(12))),
            child: LineChart(LineChartData(
              backgroundColor: Colors.transparent,
              borderData: FlBorderData(show: false),
              gridData: FlGridData(show: false),
              lineTouchData: LineTouchData(enabled: false),
              titlesData: FlTitlesData(
                show: true,
                bottomTitles: SideTitles(
                  textStyle: TextStyle(color: Colors.white),
                  showTitles: true,
                  reservedSize: 0,
                  interval: 2,
                  margin: 30,
                ),
                leftTitles: SideTitles(
                  getTitles: (value) {
                    return value.toStringAsFixed(0);
                  },
                  showTitles: true,
                  margin: 30,
                  reservedSize: 1,
                  textStyle: TextStyle(color: Colors.white),
                ),
              ),
              lineBarsData: [
                LineChartBarData(
                  spots: spots,
                  colors: [Colors.white],
                  isCurved: true,
                  barWidth: 1,
                  dashArray: [4],
                  belowBarData: BarAreaData(show: false),
                  dotData: FlDotData(show: true),
                ),
              ],
            )),
          ),
        ],
      ),
    );
  }
}
