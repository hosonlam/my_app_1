import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_app_1/utils/icon.dart';
import 'package:sensors/sensors.dart';
import 'package:flutter_compass/flutter_compass.dart';
import 'package:flare_flutter/flare.dart';
import 'package:flare_flutter/flare_controls.dart';
import 'package:flare_flutter/flare_actor.dart';

class Anim {
  String name;
  double _value = 0, pos = 0, min, max, speed;
  bool endless = false;
  ActorAnimation actor;
  Anim(this.name, this.min, this.max, this.speed, this.endless);
  get value => _value * (max - min) + min;
  set value(double v) => _value = (v - min) / (max - min);
}

class AniControl extends FlareControls {
  List<Anim> items;
  AniControl(this.items);

  @override
  bool advance(FlutterActorArtboard board, double elapsed) {
    super.advance(board, elapsed);
    for (var a in items) {
      if (a.actor == null) continue;
      var d = (a.pos - a._value).abs();
      var m = a.pos > a._value ? -1 : 1;
      if (a.endless && d > 0.5) {
        m = -m;
        d = 1.0 - d;
      }
      var e = elapsed / a.actor.duration * (1 + d * a.speed);
      a.pos = e < d ? (a.pos + e * m) : a._value;
      if (a.endless) a.pos %= 1.0;
      a.actor.apply(a.actor.duration * a.pos, board, 1.0);
    }
    return true;
  }

  @override
  void initialize(FlutterActorArtboard board) {
    super.initialize(board);
    items.forEach((a) => a.actor = board.getAnimation(a.name));
  }

  operator [](String name) {
    for (var a in items) if (a.name == name) return a;
  }
}

class CompassScreen extends StatefulWidget {
  @override
  _CompassScreenState createState() => _CompassScreenState();
}

class _CompassScreenState extends State<CompassScreen> {
  int mode = 0, map = 0;
  AniControl compass;

  @override
  void initState() {
    super.initState();

    compass = AniControl([
      Anim('dir', 0, 360, 20, true),
      Anim('hor', -9.6, 9.6, 20, false),
      Anim('ver', -9.6, 9.6, 20, false),
    ]);

    FlutterCompass.events.listen((angle) {
      compass['dir'].value = angle;
    });

    accelerometerEvents.listen((event) {
      compass['hor'].value = -event.x;
      compass['ver'].value = -event.y;
    });
  }

  Widget _compass() {
    return GestureDetector(
      onTap: () => setState(() => mode++),
      child: Container(
        height: MediaQuery.of(context).size.width - 50,
        child: FlareActor("assets/images/compass.flr",
            fit: BoxFit.fitHeight,
            animation: 'mode${mode % 2}',
            controller: compass),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
        backgroundColor: Colors.black,
        body: StreamBuilder<double>(
            stream: FlutterCompass.events,
            builder: (context, snapshot) {
              double direction = snapshot?.data ?? 0;
              var directionName = getDirection(direction);
              return SafeArea(
                child: Stack(
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        _compass(),
                        SizedBox(height: 20),
                        Text(
                          'Độ : ${direction?.toStringAsFixed(0) ?? "0"}  \u0366',
                          style: TextStyle(color: Colors.white, fontSize: 18),
                        ),
                        SizedBox(height: 10),
                        Text(
                          'Hướng : ${directionName ?? "Bắc"}',
                          style: TextStyle(color: Colors.white, fontSize: 18),
                        ),
                      ],
                    ),
                    Positioned(
                      top: 20,
                      right: 20,
                      child: InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          width: 30,
                          height: 30,
                          child: Icon(
                            MyIcon.close,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              );
            }));
  }

  String getDirection(double direction) {
    if (direction < 5 || direction > 355) {
      return tr("compass_N");
    }

    if (direction > 85 && direction < 95) {
      return tr("compass_E");
    }

    if (direction > 175 && direction < 185) {
      return tr("compass_S");
    }

    if (direction > 265 && direction < 275) {
      return tr("compass_W");
    }

    if (direction > 0 && direction <= 25) {
      return tr("compass_NNE");
    }

    if (direction > 25 && direction <= 70) {
      return tr("compass_NE");
    }

    if (direction > 70 && direction < 90) {
      return tr("compass_EEN");
    }

    if (direction > 90 && direction <= 115) {
      return tr("compass_EES");
    }

    if (direction > 115 && direction <= 160) {
      return tr("compass_ES");
    }

    if (direction > 160 && direction < 180) {
      return tr("compass_SSE");
    }

    if (direction > 180 && direction <= 205) {
      return tr("compass_SSW");
    }

    if (direction > 205 && direction <= 250) {
      return tr("compass_SW");
    }

    if (direction > 250 && direction < 270) {
      return tr("compass_WWS");
    }

    if (direction > 270 && direction <= 295) {
      return tr("compass_WWN");
    }

    if (direction > 295 && direction <= 340) {
      return tr("compass_WN");
    }

    if (direction > 340 && direction < 360) {
      tr("compass_NNW");
    }

    return "";
  }
}
