import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:my_app_1/bloc/covid_bloc/covid_bloc.dart';
import 'package:my_app_1/screen/base/frame.dart';
import 'package:my_app_1/widget/clipper_custom.dart';
import 'package:my_app_1/widget/loading.dart';
import 'package:my_app_1/widget/popup_dialog.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:url_launcher/url_launcher.dart';

class CovidScreen extends StatefulWidget {
  @override
  _CovidScreenState createState() => _CovidScreenState();
}

class _CovidScreenState extends State<CovidScreen> {
  final controller = ScrollController();
  double offset = 0;
  CovidBloc bloc = CovidBloc();
  RefreshController _refreshController = RefreshController(initialRefresh: false);

  @override
  void initState() {
    super.initState();
    controller.addListener(onScroll);
    bloc.add(LoadDataEvent());
  }

  @override
  void dispose() {
    controller.dispose();
    bloc.close();
    super.dispose();
  }

  void onScroll() {
    setState(() {
      offset = (controller.hasClients) ? controller.offset : 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return BlocProvider(
        create: (context) => bloc,
        child: BlocListener<CovidBloc, CovidState>(listener: (context, state) {
          if (state is ErrorCovidState) {
            if (state.message != null) {
              DialogUtilities.showError(context, state.message, isLock: true);
            }
          }
        }, child: BlocBuilder<CovidBloc, CovidState>(builder: (context, state) {
          if (state is LoadedCovidState) {
            return getMainWidget(state, controller, offset);
          } else {
            return Container(
              color: Colors.white,
            );
          }
        })));
  }

  Widget getMainWidget(
      LoadedCovidState state, dynamic controller, dynamic offset) {
    if (state.loading != null && state.loading == true) {
      return LoadingWidget();
    } else {
      return Scaffold(
        body: SmartRefresher(
          controller: _refreshController,
          onRefresh: _onRefresh,
          onLoading: _onLoading,
          enablePullDown: true,
          header: WaterDropHeader(),
          child: SingleChildScrollView(
            controller: controller,
            child: Column(
              children: <Widget>[
                _header(
                    image: "assets/icons/Drcorona.svg",
                    offset: offset,
                    context: context),
                SizedBox(height: 20),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                    text: "${tr("covid_today")} ${state.data.todayCases}/${state.data.todayDeaths}\n",
                                    style: TextStyle(color: Colors.black)),
                                TextSpan(
                                    text:
                                        "${tr("covid_day")} ${DateTime.now().day}/${DateTime.now().month}/${DateTime.now().year}",
                                    style: TextStyle(color: Colors.black)),
                                    
                              ],
                            ),
                          ),
                          Spacer(),
                          InkWell(
                            onTap: () {
                              launch('https://ncov.moh.gov.vn/');
                            },
                            child: Text(
                              tr("covid_detail"),
                              style: TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 20),
                      Container(
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0, 4),
                              blurRadius: 30,
                              color: Colors.black12,
                            ),
                          ],
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            _counter(
                              color: Colors.red,
                              number: state.data.cases,
                              title: tr("covid_infected"),
                            ),
                            _counter(
                              color: Colors.black,
                              number: state.data.deaths,
                              title: tr("covid_death"),
                            ),
                            _counter(
                              color: Colors.green,
                              number: state.data.recovered,
                              title: tr("covid_recovery"),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 30),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            tr("covid_map"),
                          ),
                          InkWell(
                            onTap: () {
                              launch('https://ncov.moh.gov.vn/ban-do-vn');
                            },
                            child: Text(
                              tr("covid_detail"),
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: Colors.blue),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        padding: EdgeInsets.all(20),
                        width: double.infinity,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0, 10),
                              blurRadius: 30,
                              color: Colors.black12,
                            ),
                          ],
                        ),
                        child: Image.asset(
                          "assets/images/vietnam.png",
                          fit: BoxFit.contain,
                        ),
                      ),
                      SizedBox(height: 20),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
  }

  Widget _counter({int number, Color color, String title}) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(6),
          height: 25,
          width: 25,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: color.withOpacity(.26),
          ),
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.transparent,
              border: Border.all(
                color: color,
                width: 2,
              ),
            ),
          ),
        ),
        SizedBox(height: 10),
        Text(
          "$number",
          style: TextStyle(
            fontSize: 40,
            color: color,
          ),
        ),
        Text(title),
      ],
    );
  }

  Widget _header({BuildContext context, String image, double offset}) {
    return ClipPath(
      clipper: ClippperCustom(),
      child: Container(
        padding: EdgeInsets.only(left: 40, top: 50, right: 20),
        height: 350,
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [
              Colors.green[300],
              Colors.green[500],
              Colors.green[700],
              Colors.green[900],
            ],
          ),
          image: DecorationImage(
            image: AssetImage("assets/images/virus.png"),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            InkWell(
                onTap: () {
                  setState(() {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Frame()));
                  });
                },
                child: Icon(
                  Icons.close,
                  size: 30,
                  color: Colors.white,
                )),
            SizedBox(height: 20),
            Expanded(
              child: Stack(
                children: <Widget>[
                  Positioned(
                    top: (offset < 0) ? 0 : offset,
                    child: SvgPicture.asset(
                      image,
                      width: 230,
                      fit: BoxFit.fitWidth,
                      alignment: Alignment.topCenter,
                    ),
                  ),
                  Positioned(
                    top: 20 - offset / 2,
                    left: 150,
                    child: Text(
                      tr("covid_slogan"),
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  Container(), // I dont know why it can't work without container
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    if (mounted)
      setState(() {
        bloc.add(LoadDataEvent());
      });
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    await Future.delayed(Duration(milliseconds: 1000));
    _refreshController.loadComplete();
  }
}
