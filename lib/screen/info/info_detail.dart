import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:my_app_1/bloc/info_bloc/info_bloc.dart';
import 'package:my_app_1/constant.dart';
import 'package:my_app_1/model/customer.dart';
import 'package:my_app_1/screen/base/frame.dart';
import 'package:my_app_1/utils/icon.dart';
import 'package:my_app_1/utils/static.dart';

class InfoDetailScreen extends StatefulWidget {
  final isNew;
  final CustomerModel model;
  final InfoBloc bloc;

  const InfoDetailScreen({Key key, this.model, this.bloc, this.isNew})
      : super(key: key);

  @override
  State createState() => _InfoDetailScreenState();
}

class _InfoDetailScreenState extends State<InfoDetailScreen> {
  DateTime _currentTime = DateTime.utc(2000, 6, 15);
  bool isMan = false;
  ScrollController _scrollController;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    if (!widget.isNew) {
      widget.bloc.add(LoadDetailEvent(widget.model));
    } else {
      widget.bloc.add(NewDataEvent());
    }
    isMan = widget.model?.sex == 1 ?? 1;
    _scrollController = ScrollController(
      initialScrollOffset: 500,
      keepScrollOffset: true,
    );
  }

  @override
  void dispose() {
    super.dispose();
    widget.bloc.close();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      //APPBAR
      appBar: AppBar(
        backgroundColor: Season.getColors[StaticVariable.season],
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            setState(() {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Frame()));
            });
          },
        ),
        centerTitle: true,
        title: Text("${widget.model?.fullName ?? tr("contact_add")}"),
        elevation: 0,
      ),
      //BODY
      body: BlocProvider(
          create: (context) => widget.bloc,
          child: BlocListener<InfoBloc, InfoState>(listener: (context, state) {
            if (state is LoadedDetailState) {}
          }, child: BlocBuilder<InfoBloc, InfoState>(builder: (context, state) {
            if (state is LoadedDetailState) {
              var model = state.data;
              return _mainWidget(model);
            }
            if (state is LoadedCreateState) {
              var model = state.data;
              return _mainWidget(model);
            }
            return Container();
          }))),
    );
  }

  Widget _mainWidget(CustomerModel model) {
    final bottom = MediaQuery.of(context).viewInsets.bottom;
    return SingleChildScrollView(
      controller: _scrollController,
      reverse: true,
      child: Column(
        children: <Widget>[
          Form(
            key: _formKey,
            child: Container(
              padding: EdgeInsets.only(bottom: bottom),
              child: Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    //Giới tính
                    _fieldSex(model),
                    SizedBox(height: 20),
                    //Ngày sinh
                    _fieldBirthDay(model),
                    SizedBox(height: 30),
                    //Họ tên
                    TextFormField(
                      style: TextStyle(fontSize: 24),
                      decoration: InputDecoration(
                          counterText: "",
                          labelText: tr("customer_name_title"),
                          labelStyle: TextStyle(fontSize: 18),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(16)),
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon: Icon(Icons.perm_identity)),
                      keyboardType: TextInputType.text,
                      initialValue: model?.fullName ?? "",
                      maxLength: 30,
                      onFieldSubmitted: (value) {
                        model.fullName = value;
                        widget.bloc.add(DetailChangeEvent(model, !widget.isNew));
                      },
                      validator: (value) {
                        if (value.isEmpty) {
                          return tr("contact_validate_message");
                        }
                        return null;
                      },
                    ),
                    //Tuổi
                    SizedBox(height: 30),
                    TextFormField(
                      enabled: false,
                      key: Key(model?.birthDay.toString()),
                      style: TextStyle(fontSize: 24),
                      decoration: InputDecoration(
                          counterText: "",
                          labelText: tr("customer_age_title"),
                          labelStyle: TextStyle(fontSize: 18),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black54),
                              borderRadius: BorderRadius.circular(16)),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(16)),
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon: Icon(Icons.email)),
                      keyboardType: TextInputType.text,
                      initialValue: model?.age ?? "",
                    ),
                    //Số điện thoại 1
                    SizedBox(height: 30),
                    TextFormField(
                      style: TextStyle(fontSize: 24),
                      decoration: InputDecoration(
                          counterText: "",
                          labelText: tr("customer_phone1_title"),
                          labelStyle: TextStyle(fontSize: 18),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(16)),
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon: Icon(Icons.phone_android)),
                      keyboardType: TextInputType.text,
                      initialValue: model?.phone ?? "",
                      maxLength: 11,
                      onFieldSubmitted: (value) {
                        model.phone = value;
                        widget.bloc.add(DetailChangeEvent(model, !widget.isNew));
                      },
                      validator: (value) {
                        if (value.isEmpty) {
                          return tr("contact_validate_message");
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 30),
                    //Số điện thoại 2
                    TextFormField(
                      style: TextStyle(fontSize: 24),
                      decoration: InputDecoration(
                          counterText: "",
                          labelText: tr("customer_phone2_title"),
                          labelStyle: TextStyle(fontSize: 18),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(16)),
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon: Icon(Icons.phone)),
                      keyboardType: TextInputType.text,
                      initialValue: model?.phone2 ?? "",
                      maxLength: 11,
                      onFieldSubmitted: (value) {
                        model.phone2 = value;
                        widget.bloc.add(DetailChangeEvent(model, !widget.isNew));
                      },
                      validator: (value) {
                        if (value.isEmpty) {
                          return tr("contact_validate_message");
                        }
                        return null;
                      },
                    ),
                    //Email
                    SizedBox(height: 30),
                    TextFormField(
                      style: TextStyle(fontSize: 24),
                      decoration: InputDecoration(
                          counterText: "",
                          labelText: tr("customer_email_title"),
                          labelStyle: TextStyle(fontSize: 18),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(16)),
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon: Icon(Icons.email)),
                      keyboardType: TextInputType.text,
                      initialValue: model?.email ?? "",
                      maxLength: 50,
                      onFieldSubmitted: (value) {
                        model.email = value;
                        widget.bloc.add(DetailChangeEvent(model, !widget.isNew));
                      },
                      validator: (value) {
                        if (value.isEmpty) {
                          return tr("contact_validate_message");
                        }
                        return null;
                      },
                    ),
                    //Địa chỉ
                    SizedBox(height: 30),
                    TextFormField(
                      style: TextStyle(fontSize: 24),
                      decoration: InputDecoration(
                          counterText: "",
                          labelText: tr("customer_address_title"),
                          labelStyle: TextStyle(fontSize: 18),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(16)),
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon: Icon(Icons.map)),
                      keyboardType: TextInputType.text,
                      initialValue: model?.address ?? "",
                      maxLength: 50,
                      onFieldSubmitted: (value) {
                        model.address = value;
                        widget.bloc.add(DetailChangeEvent(model, !widget.isNew));
                      },
                      validator: (value) {
                        if (value.isEmpty) {
                          return tr("contact_validate_message");
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 30),
                    //Chứng minh
                    TextFormField(
                      style: TextStyle(fontSize: 24),
                      decoration: InputDecoration(
                          counterText: "",
                          labelText: tr("customer_id_title"),
                          labelStyle: TextStyle(fontSize: 18),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(16)),
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon: Icon(Icons.credit_card)),
                      keyboardType: TextInputType.text,
                      initialValue: model?.idCard ?? "",
                      maxLength: 12,
                      onFieldSubmitted: (value) {
                        model.idCard = value;
                        widget.bloc.add(DetailChangeEvent(model, !widget.isNew));
                      },
                      validator: (value) {
                        if (value.isEmpty) {
                          return tr("contact_validate_message");
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 30),
                    //Nơi sinh
                    TextFormField(
                      style: TextStyle(fontSize: 24),
                      decoration: InputDecoration(
                          counterText: "",
                          labelText: tr("customer_birthplace_title"),
                          labelStyle: TextStyle(fontSize: 18),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(16)),
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon: Icon(Icons.place)),
                      keyboardType: TextInputType.text,
                      initialValue: model?.birthPlace ?? "",
                      maxLength: 100,
                      onFieldSubmitted: (value) {
                        model.birthPlace = value;
                        widget.bloc.add(DetailChangeEvent(model, !widget.isNew));
                      },
                      validator: (value) {
                        if (value.isEmpty) {
                          return tr("contact_validate_message");
                        }
                        return null;
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 30),
          FlatButton(
            child: Container(
              height: 50,
              width: 200,
              decoration: BoxDecoration(
                  color: Colors.green, borderRadius: BorderRadius.circular(30)),
              child: Icon(
                MyIcon.save,
                color: Colors.white,
              ),
            ),
            onPressed: () {
              if (_formKey.currentState.validate()) {
                setState(() {
                  if (widget.isNew) {
                    widget.bloc.add(AddDataEvent(model: model));
                  } else {
                    widget.bloc.add(SaveDataEvent(model: model));
                  }
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Frame()));
                });
              }
            },
          ),
          SizedBox(height: 30),
        ],
      ),
    );
  }

  Widget _fieldBirthDay(dynamic model) {
    return InkWell(
      child: Container(
        width: double.infinity,
        child: Column(
          children: <Widget>[
            Text("Ngày sinh", style: TextStyle(color: Colors.black45)),
            SizedBox(height: 10),
            Container(
              height: 70,
              padding: const EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(18),
                  border: Border.all(color: Colors.black54)),
              child: Row(
                children: <Widget>[
                  Icon(Icons.calendar_today, color: Colors.black45),
                  SizedBox(width: 10),
                  Text(
                    '${model?.birthDay ?? "Ngày sinh"}',
                    style: TextStyle(fontSize: 24),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
        DatePicker.showDatePicker(
          context,
          showTitleActions: true,
          currentTime: _currentTime,
          maxTime: DateTime.now(),
          locale: LocaleType.vi,
          theme: DatePickerTheme(
              containerHeight: 300,
              headerColor: Colors.green[300],
              backgroundColor: Colors.green,
              itemStyle: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
              doneStyle: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.bold),
              cancelStyle: TextStyle(color: Colors.white, fontSize: 16)),
          onConfirm: (time) {
            setState(() {
              model.birthDay = DateFormat("dd/MM/yyyy").format(time);
              model.age = (DateTime.now().year - time.year).toString();
              widget.bloc.add(DetailChangeEvent(model, !widget.isNew));
            });
          },
        );
      },
    );
  }

  Widget _fieldSex(dynamic model) {
    return Container(
      padding: const EdgeInsets.only(bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Giới tính', style: TextStyle(color: Colors.black45)),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              InkWell(
                  onTap: () {
                    setState(() {
                      isMan = true;
                      model.sex = 1;
                    });
                  },
                  child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(45),
                        color: isMan
                            ? Color.fromRGBO(43, 204, 53, 1)
                            : Color.fromRGBO(194, 255, 198, 1),
                      ),
                      width: 150,
                      height: 50,
                      child: Icon(MyIcon.male,
                          color: !isMan
                              ? Color.fromRGBO(43, 204, 53, 1)
                              : Colors.white))),
              InkWell(
                  onTap: () {
                    setState(() {
                      isMan = false;
                      model.sex = 0;
                    });
                  },
                  child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(45),
                        color: isMan
                            ? Color.fromRGBO(250, 202, 223, 1)
                            : Color.fromRGBO(227, 61, 134, 1),
                      ),
                      width: 150,
                      height: 50,
                      child: Icon(MyIcon.female,
                          color: isMan
                              ? Color.fromRGBO(227, 61, 134, 1)
                              : Colors.white)))
            ],
          ),
        ],
      ),
    );
  }
}
