import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:my_app_1/bloc/info_bloc/info_bloc.dart';
import 'package:my_app_1/model/customer.dart';
import 'package:my_app_1/screen/info/info_detail.dart';
import 'package:my_app_1/utils/icon.dart';
import 'package:my_app_1/widget/alert_dialog.dart';
import 'package:my_app_1/widget/loading_state.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:my_app_1/utils/static.dart';
import 'package:my_app_1/constant.dart';
import 'dart:math';

class InfoScreen extends StatefulWidget {
  @override
  _InfoScreenState createState() => _InfoScreenState();
}

class _InfoScreenState extends State<InfoScreen> {
  InfoBloc bloc = InfoBloc();

  @override
  void initState() {
    super.initState();
    bloc.add(LoadDataEvent());
  }

  @override
  void dispose() {
    super.dispose();
    bloc.close();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: BlocProvider(
          create: (context) => bloc,
          child: BlocListener<InfoBloc, InfoState>(
              listener: (context, state) {
            if (state is LoadedInfoState) {}
          }, child:
                  BlocBuilder<InfoBloc, InfoState>(builder: (context, state) {
            if (state is LoadedInfoState) {
              return getMainWidget(state);
            }
            return Container();
          }))),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Season.getColors[StaticVariable.season],
        onPressed: () {
          setState(() {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => InfoDetailScreen(
                          bloc: bloc,
                          isNew: true,
                        )));
          });
        },
        icon: Icon(MyIcon.add),
        label: Text(
          tr("contact_add"),
        ),
      ),
    );
  }

  Widget getMainWidget(LoadedInfoState state) {
    var data = state.datas;

    if (state.loading != null && state.loading == true) {
      return Stack(
        children: [
          Container(
              color: Colors.white.withOpacity(0.3), child: LoadingStateWidget())
        ],
      );
    } else {
      return Container(
        child: ListView.builder(
          itemCount: state.datas?.length ?? 0,
          itemBuilder: (BuildContext context, int index) {
            return ListCard(
              data: data,
              index: index,
              bloc: bloc,
            );
          },
        ),
      );
    }
  }
}

class ListCard extends StatelessWidget {
  const ListCard({
    Key key,
    @required this.data,
    this.index,
    this.bloc,
  }) : super(key: key);

  final List<CustomerModel> data;
  final int index;
  final InfoBloc bloc;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: 0.2,
      actions: <Widget>[
        IconSlideAction(
          caption: tr("contact_del"),
          color: Colors.red,
          icon: Icons.delete_forever,
          onTap: () => ShowAlertDialog.twoButton(
              context: context,
              title: tr("contact_alert"),
              message: tr("contact_del_message"),
              agree: tr("contact_agree"),
              disagree: tr("contact_disagree"),
              onPressed: () {
                bloc.add(DeleteEvent(data[index]));
                Navigator.pop(context);
              }),
        ),
        IconSlideAction(
          caption: tr("contact_edit"),
          color: Colors.blue,
          icon: Icons.edit,
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => InfoDetailScreen(
                          model: data[index],
                          bloc: bloc,
                          isNew: false,
                        )));
          },
        ),
      ],
      child: InkWell(
        onLongPress: () {
          ShowAlertDialog.twoButton(
              context: context,
              title: tr("contact_alert"),
              message: tr("contact_call_message"),
              agree: tr("contact_agree"),
              disagree: tr("contact_disagree"),
              onPressed: () {
                launch("tel://${data[index].phone}");
              });
        },
        child: Container(
            padding: const EdgeInsets.all(4),
            child: ListTile(
              leading: CircleAvatar(
                backgroundColor: Colors.primaries[Random().nextInt(Colors.primaries.length)],
                child: Text(
                    '${data[index].fullName?.toUpperCase()?.substring(0, 1) ?? ""}'),
                foregroundColor: Colors.white,
              ),
              title: Text(
                '${data[index].fullName}',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              subtitle: Text(
                '${tr("contact_phone_number")} ${data[index].phone}',
                style: TextStyle(fontSize: 14),
              ),
            )),
      ),
    );
  }
}
