import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_app_1/database/master_data.dart';
import 'package:my_app_1/screen/login/login.dart';
import 'package:my_app_1/utils/season_calculate.dart';
import 'package:my_app_1/utils/static.dart';
import 'package:permission_handler/permission_handler.dart';

void main() async {
  runApp(SplashScreen());
  final database = MasterData();
  WidgetsFlutterBinding.ensureInitialized();
  var status = await Permission.storage.status;
  if (!status.isGranted) {
    await Permission.storage.request();
  }

  var geolocationStatus = await Permission.location.status;
  if (!geolocationStatus.isGranted) {
    await Permission.location.request();
  }
  await database.open();
  // var fireBase = await initializeFlutterFire();

  // if (fireBase) {
  //   return SomethingWentWrong();
  // }

  SystemChrome.setEnabledSystemUIOverlays([]);
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  StaticVariable.season = SeasonCalculate().getSeason();
  await Future.delayed(Duration(seconds: 3));
  runApp(
    EasyLocalization(
        supportedLocales: [Locale('vi', 'VN')],
        path: 'assets/translations',
        fallbackLocale: Locale('vi', 'VN'),
        child: MyApp()),
  );
}

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: Scaffold(
          backgroundColor: Colors.white,
          body: Center(child: Image.asset('assets/images/laptop.gif')),
        ));
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: LoginScreen());
  }
}
