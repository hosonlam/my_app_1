part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}

class LoadDataEvent extends LoginEvent {
  LoadDataEvent();
}

class ValidatedEvent extends LoginEvent {
  final String id;
  final String password;
  ValidatedEvent(this.id, this.password);
}
