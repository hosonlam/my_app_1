import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:my_app_1/model/account.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(LoginInitial());
  AccountModel data;
  AccountProvider account = AccountProvider();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is ValidatedEvent) {
      yield* _validate(event);
    } else if (event is LoadDataEvent) {
      yield LoadedLoginState();
    }
  }

  Stream<LoginState> _validate(ValidatedEvent event) async* {
    yield LoadedLoginState(loading: true);
    if (event.id == null) {
      yield LoadedLoginState(
          login: false, message: "Vui lòng nhập tài khoản !");
    }
    if (event.password == null) {
      yield LoadedLoginState(
          login: false, message: "Vui lòng nhập tài khoản !");
    } else {
      data = await account.getAccountbyId(event.id);
      if (data != null) {
        if (data.password == event.password) {
          yield LoadedLoginState(login: true);
        } else {
          yield LoadedLoginState(login: false, message: "Sai mật khẩu !");
        }
      } else {
        yield LoadedLoginState(
            login: false, message: "Tài khoản không tồn tại");
      }
    }
  }
}
