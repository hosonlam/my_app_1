part of 'login_bloc.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {}

class LoadedLoginState extends LoginState {
  final bool loading;
  final bool login;
  final String message;
  LoadedLoginState({this.loading,this.login,this.message});
}