part of 'weather_bloc.dart';

@immutable
abstract class WeatherEvent {}

class LoadDataEvent extends WeatherEvent {
  final bool isRefresh;
  LoadDataEvent({this.isRefresh = false});
}
