part of 'weather_bloc.dart';

@immutable
abstract class WeatherState {}

class WeatherInitial extends WeatherState {}

class LoadedWeatherState extends WeatherState {
  final bool loading;
  final WeatherModel data;
  final WeatherForecast dataF;
  final String path;
  LoadedWeatherState({this.path, this.data,this.dataF ,this.loading});
}

class ErrorWeatherState extends WeatherState {
  final String message;
  ErrorWeatherState({this.message});
}
