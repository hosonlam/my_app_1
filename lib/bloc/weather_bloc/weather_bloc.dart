import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:geolocator/geolocator.dart';
import 'package:my_app_1/constant.dart';
import 'package:my_app_1/model/ip.dart';
import 'package:my_app_1/model/weather.dart';
import 'package:my_app_1/services/location_services.dart';
import 'package:my_app_1/services/weather_services.dart';
import 'package:my_app_1/services/ip_services.dart';
import 'package:permission_handler/permission_handler.dart';

part 'weather_event.dart';
part 'weather_state.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  WeatherBloc() : super(WeatherInitial());
  WeatherModel data;
  WeatherForecast dataF;
  IpModel dataIp;
  Position position;

  @override
  Stream<WeatherState> mapEventToState(WeatherEvent event) async* {
    if (event is LoadDataEvent) {
      yield* _loadData(event);
    }
  }

  Stream<WeatherState> _loadData(LoadDataEvent event) async* {
    yield LoadedWeatherState(loading: true);
    var path, condition, lat, long;
    var geolocationStatus = await Permission.location.status;
    var status = await Geolocator().isLocationServiceEnabled();
    Geolocator()..forceAndroidLocationManager = true;
  
    if (!geolocationStatus.isGranted && !geolocationStatus.isPermanentlyDenied) {
      await Permission.location.request();
    } else if (geolocationStatus.isPermanentlyDenied) {
      yield ErrorWeatherState(message: "Vui lòng cấp quyền định vị");
      return;
    }
    if (!status) {
      LocationServices().openLocationSetting();
      yield ErrorWeatherState(message: null);
    }
    
    await Geolocator().getCurrentPosition().timeout(Duration(seconds: 3)).then((posittion){
      lat = position.latitude.toString();
      long = position.longitude.toString();
    }).catchError((error) async {
      if(error is TimeoutException){
        position = position = await Geolocator().getLastKnownPosition();
      }
    });

    if(position == null || lat == null || long == null){
      var resultIp =  await IpServices().getExternalIp();
      dataIp = IpModel.fromJson(resultIp);
      lat = dataIp.location.lat.toString();
      long = dataIp.location.lng.toString();
    }
   
    var result = await WeatherService().getWeatherData(lat, long, event.isRefresh);
    var resultF = await WeatherService().getForecastData(lat, long, event.isRefresh);
    data = WeatherModel.fromJson(result);
    dataF = WeatherForecast.fromJson(resultF);
    condition = data.weather.first.main.toLowerCase();
    path = WeatherCondition.getBackground[condition];

    if (data.weather.first.id > 700 && data.weather.first.id < 800) {
      path = WeatherCondition.getBackground[WeatherCondition.atmosphere];
    }
    yield LoadedWeatherState(data: data, dataF: dataF, path: path);
  }
}
