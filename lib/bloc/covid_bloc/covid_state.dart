part of 'covid_bloc.dart';

@immutable
abstract class CovidState {}

class CovidInitial extends CovidState {}

class LoadedCovidState extends CovidState {
  final bool loading;
  final CovidModel data;
  LoadedCovidState({this.data,this.loading});
}

class ErrorCovidState extends CovidState {
  final String message;
  ErrorCovidState({this.message});
}
