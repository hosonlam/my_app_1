import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:my_app_1/model/covid.dart';
import 'package:my_app_1/services/covid_services.dart';

part 'covid_event.dart';
part 'covid_state.dart';

class CovidBloc extends Bloc<CovidEvent, CovidState> {
  CovidBloc() : super(CovidInitial());
  CovidModel data;

  @override
  Stream<CovidState> mapEventToState(CovidEvent event) async* {
    if (event is LoadDataEvent) {
      yield* _loadData(event);
    }
  }

  Stream<CovidState> _loadData(LoadDataEvent event) async* {
    yield LoadedCovidState(loading: true);
    var result = await CovidServices().getCovidData;
    data = CovidModel.fromJson(result);
    yield LoadedCovidState(data: data);
  }
}
