part of 'covid_bloc.dart';

@immutable
abstract class CovidEvent {}

class LoadDataEvent extends CovidEvent {
  LoadDataEvent();
}
