part of 'account_bloc.dart';

@immutable
abstract class AccountEvent {}

class LoadDataEvent extends AccountEvent {
  LoadDataEvent();
}

class LoadDetailEvent extends AccountEvent {
  final AccountStoreModel model;
  LoadDetailEvent(this.model);
}

class DetailChangeEvent extends AccountEvent {
  final AccountStoreModel model;
  DetailChangeEvent(this.model);
}

class SaveDataEvent extends AccountEvent {
  final AccountStoreModel model;
  SaveDataEvent({this.model});
}

class NewDataEvent extends AccountEvent {
  final AccountStoreModel model;
  NewDataEvent({this.model});
}

class AddDataEvent extends AccountEvent {
  final AccountStoreModel model;
  AddDataEvent({this.model});
}

class DeleteEvent extends AccountEvent {
  final AccountStoreModel model;
  DeleteEvent(this.model);
}
