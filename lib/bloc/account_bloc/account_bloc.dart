import 'package:flutter_bloc/flutter_bloc.dart';
import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:my_app_1/model/account_store.dart';

part 'account_event.dart';
part 'account_state.dart';

class AccountBloc extends Bloc<AccountEvent, AccountState> {
  AccountBloc() : super(InfoInitial());
  List<AccountStoreModel> datas;
  AccountStoreModel data;
  AccountStoreProvider account = AccountStoreProvider();

  @override
  Stream<AccountState> mapEventToState(AccountEvent event) async* {
    if (event is LoadDataEvent) {
      yield* _loadData();
    } else if (event is SaveDataEvent) {
      yield* _saveData(event);
    } else if (event is LoadDetailEvent) {
      yield* _loadDetailData(event);
    } else if (event is DetailChangeEvent) {
      yield* _detailChange(event);
    } else if (event is NewDataEvent) {
      yield* _newData();
    } else if (event is AddDataEvent) {
      yield* _addData(event);
    } else if (event is DeleteEvent) {
      yield* _deleteData(event);
    }
  }

  Stream<AccountState> _loadData() async* {
    yield LoadedAccountState(loading: true, datas: datas);
    datas = await account.getAll();
    yield LoadedAccountState(datas: datas);
  }

  Stream<AccountState> _loadDetailData(LoadDetailEvent event) async* {
    data = await account.getAccountById(event.model.id);
    if (data != null) {
      yield LoadedDetailState(data: data);
    } else {
      yield ErrorAccountState(message: 'Không có dữ liệu !');
    }
  }

  Stream<AccountState> _detailChange(DetailChangeEvent event) async* {
    data = event.model;
    yield LoadedDetailState(data: data);
  }

  Stream<AccountState> _saveData(SaveDataEvent event) async* {
    if (event.model != null) {
      account.update(event.model);
      datas = await account.getAll();
      yield LoadedAccountState(datas: datas);
    } else {
      yield ErrorAccountState(message: 'Lưu dữ liệu bị lỗi!');
    }
  }

  Stream<AccountState> _newData() async* {
    AccountStoreModel data = new AccountStoreModel();
    yield LoadedDetailState(data: data);
  }

  Stream<AccountState> _addData(AddDataEvent event) async* {
    if (event.model != null) {
      account.insert(event.model);
      datas = await account.getAll();
      data = event.model;
      yield LoadedDetailState(data: data);
    } else {
      yield ErrorAccountState(message: 'Thêm mới bị lỗi!');
    }
  }

  Stream<AccountState> _deleteData(DeleteEvent event) async* {
    try {
      account.delete(event.model.id);
      datas = await account.getAll();
      yield LoadedAccountState(datas: datas);
    } catch (e) {
      yield ErrorAccountState(message: "Lỗi không thể xóa !");
    }
  }
}
