part of 'account_bloc.dart';

@immutable
abstract class AccountState {}

class InfoInitial extends AccountState {}

class LoadedAccountState extends AccountState {
  final bool loading;
  final List<AccountStoreModel> datas;
  LoadedAccountState({this.datas, this.loading});
}

class LoadedDetailState extends AccountState {
  final AccountStoreModel data;
  LoadedDetailState({this.data});
}

class ErrorAccountState extends AccountState {
  final String message;
  ErrorAccountState({this.message});
}
