part of 'fund_bloc.dart';

@immutable
abstract class FundEvent {}

class LoadDataEvent extends FundEvent {
  LoadDataEvent();
}

class SaveDataEvent extends FundEvent {
  final CustomerModel model;
  SaveDataEvent({this.model});
}

class NewDataEvent extends FundEvent {
  final CustomerModel model;
  NewDataEvent({this.model});
}

class AddDataEvent extends FundEvent {
  final CustomerModel model;
  AddDataEvent(this.model);
}

class DeleteEvent extends FundEvent {
  final CustomerModel model;
  DeleteEvent(this.model);
}
