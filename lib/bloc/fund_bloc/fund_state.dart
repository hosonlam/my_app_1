part of 'fund_bloc.dart';

@immutable
abstract class FundState {}

class FundInitial extends FundState {}

class LoadedFundState extends FundState {
  final bool loading;
  final List<FundModel> datas;
  final List<FundModel> chartData;
  LoadedFundState({this.chartData, this.datas, this.loading});
}

class LoadedCreateState extends FundState {
  final FundModel data;
  LoadedCreateState({this.data});
}

class ErrorFundState extends FundState {
  final String message;
  ErrorFundState({this.message});
}
