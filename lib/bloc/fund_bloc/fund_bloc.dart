import 'package:flutter_bloc/flutter_bloc.dart';
import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:my_app_1/model/customer.dart';
import 'package:my_app_1/model/fund.dart';

part 'info_event.dart';
part 'fund_state.dart';

class FundBloc extends Bloc<FundEvent, FundState> {
  FundBloc() : super(FundInitial());
  List<FundModel> datas;
  List<FundModel> chartData;
  FundModel data;
  FundProvider fund = FundProvider();

  @override
  Stream<FundState> mapEventToState(FundEvent event) async* {
    if (event is LoadDataEvent) {
      yield* _loadData();
    } else {
      yield LoadedFundState(datas: datas);
    }
  }

  Stream<FundState> _loadData() async* {
    yield LoadedFundState(loading: true, datas: datas);
    datas = await fund.getAll();
    chartData = await fund.getFundByYear(DateTime.now().year);
    yield LoadedFundState(datas: datas, chartData: chartData);
  }
}
