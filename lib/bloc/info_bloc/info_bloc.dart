import 'package:flutter_bloc/flutter_bloc.dart';
import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:my_app_1/model/customer.dart';

part 'info_event.dart';
part 'info_state.dart';

class InfoBloc extends Bloc<InfoEvent, InfoState> {
  InfoBloc() : super(InfoInitial());
  List<CustomerModel> datas;
  CustomerModel data;
  CustomerProvider customer = CustomerProvider();

  @override
  Stream<InfoState> mapEventToState(InfoEvent event) async* {
    if (event is LoadDataEvent) {
      yield* _loadData();
    } else if (event is SaveDataEvent) {
      yield* _saveData(event);
    } else if (event is LoadDetailEvent) {
      yield* _loadDetailData(event);
    } else if (event is DetailChangeEvent) {
      yield* _detailChange(event);
    } else if (event is NewDataEvent) {
      yield* _newData();
    } else if (event is AddDataEvent) {
      yield* _addData(event);
    } else if (event is DeleteEvent) {
      yield* _deleteData(event);
    } else {
      yield LoadedInfoState(datas: datas);
    }
  }

  Stream<InfoState> _loadData() async* {
    yield LoadedInfoState(loading: true, datas: datas);
    datas = await customer.getAll();
    yield LoadedInfoState(datas: datas);

  }

  Stream<InfoState> _loadDetailData(LoadDetailEvent event) async* {
    data = await customer.getCustomerbyId(event.model.id);
    if (data != null) {
      yield LoadedDetailState(data: data);
    } else {
      yield ErrorInfoState(message: 'Không có dữ liệu !');
    }
  }

  Stream<InfoState> _detailChange(DetailChangeEvent event) async* {
    data = event.model;
    if (event.isUpdate) {
      yield LoadedDetailState(data: data);
    } else {
      yield LoadedCreateState(data: data);
    }
  }

  Stream<InfoState> _saveData(SaveDataEvent event) async* {
    if (event.model != null) {
      customer.update(event.model);
      datas = await customer.getAll();
      yield LoadedInfoState(datas: datas);
    } else {
      yield ErrorInfoState(message: 'Lưu dữ liệu bị lỗi!');
    }
  }

  Stream<InfoState> _newData() async* {
    CustomerModel data = new CustomerModel();
    data.sex = 0;
    yield LoadedCreateState(data: data);
  }

  Stream<InfoState> _addData(AddDataEvent event) async* {
    if (event.model != null) {
      customer.insert(event.model);
      datas = await customer.getAll();
      yield LoadedInfoState(datas: datas);
    } else {
      yield ErrorInfoState(message: 'Thêm mới bị lỗi!');
    }
  }

  Stream<InfoState> _deleteData(DeleteEvent event) async* {
    try {
      customer.delete(event.model.id);
      datas = await customer.getAll();
      yield LoadedInfoState(datas: datas);
    } catch (e) {
      yield ErrorInfoState(message: "Lỗi không thể xóa !");
    }
  }
}
