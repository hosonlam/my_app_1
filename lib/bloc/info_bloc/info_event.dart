part of 'info_bloc.dart';

@immutable
abstract class InfoEvent {}

class LoadDataEvent extends InfoEvent {
  LoadDataEvent();
}

class LoadDetailEvent extends InfoEvent {
  final CustomerModel model;
  LoadDetailEvent(this.model);
}

class DetailChangeEvent extends InfoEvent {
  final CustomerModel model;
  final isUpdate;
  DetailChangeEvent(this.model, this.isUpdate);
}

class SaveDataEvent extends InfoEvent {
  final CustomerModel model;
  SaveDataEvent({this.model});
}

class NewDataEvent extends InfoEvent {
  final CustomerModel model;
  NewDataEvent({this.model});
}

class AddDataEvent extends InfoEvent {
  final CustomerModel model;
  AddDataEvent({this.model});
}

class DeleteEvent extends InfoEvent {
  final CustomerModel model;
  DeleteEvent(this.model);
}
