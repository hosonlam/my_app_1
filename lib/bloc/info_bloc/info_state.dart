part of 'info_bloc.dart';

@immutable
abstract class InfoState {}

class InfoInitial extends InfoState {}

class LoadedInfoState extends InfoState {
  final bool loading;
  final List<CustomerModel> datas;
  LoadedInfoState({this.datas, this.loading});
}

class LoadedDetailState extends InfoState {
  final CustomerModel data;
  LoadedDetailState({this.data});
}

class LoadedCreateState extends InfoState {
  final CustomerModel data;
  LoadedCreateState({this.data});
}

class ErrorInfoState extends InfoState {
  final String message;
  ErrorInfoState({this.message});
}
